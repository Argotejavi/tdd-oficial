[![pipeline status](https://gitlab.com/jaagirre/flask-tdd-docker/badges/master/pipeline.svg)](https://gitlab.com/jaagirre/flask-tdd-docker/commits/master)

# TDD and Continuous integration  with docker, python flask, gitlab and herokuX

En este repositorio se encuantra una pequeña practica para trabajar los conceptos necesarios para realiziar una Integración Continua de Aplicaciones Python-Flask basadas en Docker. En este práctica se trabajarán los siguientes conceptos.

- Metodología de desarrollo basada en Testeos TDD
- Validación de aplicaciones dockerizadas
- Gestión de diferentes entornos 
    - BBDD de testing, development, production
    - Variables de entorno
        - Modo debug , Modo producción 
    - Gestión de las diferentes imagenes por fases
- Validación
    - Test unitarios
    - Test de integración 
    - Seleccion de tests
    - Fixtures
    - Parametrización 
    - Mocking 
    - Cobertura
- Integración continua
    - Delivery - Generación de imagenes
    - Verificación del estilo de código 
    - Automatización del proceso de CI (BUILD, TEST, DELIVERY)
    - Despliegue de aplicaciones (Deploy)


La práctica se basa en 15 pasos, aqui teneis el link a ellos


1. Configuración de la aplicación inicial [PASO_1](./docs/GIDA_00_HASIERAKETA.md)

2. Dockeriznado aplicación base FLASK [PASO_2](docs/GIDA_01_DOCKERiZAR.md)

3. Instalando Contenedor Docker Postgress [PASO_3](docs/GIDA_02_Instalando_BBDD.md)

4. Trabajando con PyTest [PASO_4](docs/GIDA_03_PyTest.md)

5. Trabajando con PyTest [PASO_5](docs/GIDA_04_blueprintf_refactor.md)

6.  Trabajando con TDD [PASO_6](docs/GIDA_05_tdd.md)

7. Despliegue de la aplicacion [PASO_7](docs/GIDA_06_DEPLOY.md)

8. Cobertura y análisis estático de código [PASO_8](docs/GIDA_07_coverage.md)

9. Integración continua con GITLAB [PASO_9](docs/GIDA_08_CI.md)

10. Despliegue continuo con GITLAB [PASO_10](docs/GIDA_09_CD.md)

11. Implementar nuevas API /delete y /put:ID  y parametrización [PASO_11](docs/GIDA_10_newAPI.md)

12. Test Doubles, Test Mocks, Monkeypatching [PASO_12](docs/GIDA_11_mock.md)


13. EXTRA. Admin BBDD y DocumentacionSwagger [PASO_13](docs/GIDA_12.md)






