# TDD and Continuous integration  with docker, python flask, gitlab and heroku

Como ordenador utilizaremos una versión del Vagrant Basic, en este caso la instancia ubicada en **c:\Users\jaagirre\dev\basic**

Con el objetivo de utilizar Visual Code como editor y debugger se utilizará la carpeta compartida entre windows y vagrant para el desarrollo del proyecto.

Un requisito para el desarrollo python es trabajar con entornos virtuales mediante el modulo **venv,** el cual viene preinstalado a partir de n pyhton3.3.

```


```

## Creación del proyecto Flask en GNU/Linux

```bash
cd c:\Users\jaagirre\dev\basic
vagrant up dev
vagrant ssh dev
mkdir -p ci_cd/flask-tdd-docker
cd /vagrant
mkdir -p ci_cd/flask-tdd-docker/project
cd /home/vagrant/ci_cd/flask-tdd-docker/
ln -s /vagrant/ci_cd/flask-tdd-docker/project/ ./project
python3 -m venv env
source env/bin/activate
cd ./project
(env) pip install flask==1.1.1
(env) pip install Flask-RESTPlus==0.13.0
(env) pip install Werkzeug==0.16.1
(env) pip install importlib-metadata==1.0.0

```
Un vez instaladas las librerías se programa el primer recurso **/ping** en fichero __init__.py


```python
# project/__init__.py


from flask import Flask, jsonify
from flask_restplus import Resource, Api


# instantiate the app
app = Flask(__name__)

api = Api(app)


class Ping(Resource):
    def get(self):
        return {
            'status': 'success',
            'message': 'pong!'
        }


api.add_resource(Ping, '/ping')
```

Y después el arranque vía cli() creando **manage.py**

```python
# manage.py

from flask.cli import FlaskGroup

from project import app

cli = FlaskGroup(app)

if __name__ == '__main__':
    cli()
```
Y para ejecutar 
```bash
export FLASK_APP=project/__init__.py
python manage.py run -host 0.0.0.0
```


Si quisiéramos ejecutar sin cli crearíamos el siguiente fichero **main.py**

```python
from project import app
if __name__ == '__main__':
    app.run(host='0.0.0.0')
```

Y para ejecutar 
```bash
export FLASK_APP=project/__init__.py
python main.py
```

Y para validar navegar a http://10.100.199.200:5000/ping

```json
{"message": "pong!", "status": "success"}
```
Una vez tenemos la aplicación mínima desarrollada se crea el fichero de configuración. Mediante este fichero se gestionara las diferentes configuraciones de la aplicación en ñas diferentes fases de desarrollo.

```python

# project/config.py

class BaseConfig:
    TESTING = False

class DevelopmentConfig(BaseConfig):
    pass

class TestingConfig(BaseConfig):
    TESTING = True

class ProductionConfig(BaseConfig):
    pass
```

Y ahora modificamos el fichero *__init__.py* para que contemple el fichero de configuración. Para ello se agrega la siguiente línea de codigo.

```python
app.config.from_object('project.config.DevelopmentConfig') # new
```

Y ahora ejecutamos Flask en modo Debug mediante la variable de entorno FLASK_ENV, de esta manera consiguimos reloading automático.

```bash
(env) export FLASK_APP=project/__init__.py
(env) export FLASK_ENV=development
(env) python main.py
python main.py
 * Serving Flask app "project" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 244-979-948
```

Ahora finalizamos el servidor y deactivamos el entorno virtual.
Finalmente creamos el fichero de dependencias **requirements.txt** con la siguiente información

```
Flask==1.1.1
Flask-RESTPlus==0.13.0
# esto incuir por BUG de Flask-restPlus
Werkzeug==0.16.1
importlib-metadata==1.0.0
itsdangerous==2.0.1
```
Ahora ya tenemos la versión inicial de de esta practica. Antes de crear y actualizar el repo crear el fichero *.gitignore* con lo siguiente.

```
__pycache__
env
```

Y ahora crear el repo en gitlab y actualizarlo. Primero crear el repo para la práctica en [gitlab.danz.eus](https://gitlab.danz.eus).

```
cd flask-tdd-docker/
git init
git remote add origin https://gitlab.danz.eus/jaagirre/practica_ci_tdd.git
git add .
git commit -m "Initial commit"
[master (root-commit) 2e599d3] Commit inicial
 8 files changed, 233 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 README.md
 create mode 100644 doc/GIDA_00_HASIERAKETA.md
 create mode 100644 main.py
 create mode 100644 manage.py
 create mode 100644 project/__init__.py
 create mode 100644 project/config.py
 create mode 100644 requirements.txt
git push origin master
```

Y ya estamos preparados para trabajar!!
El siguiente paso es Dockerizar la aplicación [Dockerizando](doc/GIDA_01_DOCKERiZAR.md)







