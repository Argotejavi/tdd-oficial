
Inicializacion
==============

Como ordenador utilizaremos una version del Vagrant Basic, en este caso la instancia ubicada en **c:\Users\jaagirre\dev\basic**

Con el objetivo de utilizar Visual Code como editor y debugger se utilizará la carpeta compartida entre windows y vagrant para el desarrollo del proyecto.

Un requisito para el desarrollo pyhton es trabajar con entornos virtuales mediante el modulo **venv,** el cual viene preinstalado a partir de n pyhton3.3.

.. code-block::



Creación del proyecto Flask en GNU/Linux
----------------------------------------

.. code-block:: bash

   cd c:\Users\jaagirre\dev\basic
   vagrant up dev
   vagrant ssh dev
   mkdir -p ci_cd/flask-tdd-docker
   cd /vagrant
   mkdir -p ci_cd/flask-tdd-docker/project
   cd /home/vagrant/ci_cd/flask-tdd-docker/
   ln -s /vagrant/ci_cd/flask-tdd-docker/project/ ./project
   python3 -m venv env
   source env/bin/activate
   cd ./project
   (env) pip install flask==1.1.1
   (env) pip install Flask-RESTPlus==0.13.0
   (env) pip install Werkzeug==0.16.1
   (env) pip install importlib-metadata==1.0.0

Un vez instaladas las librerias se programa el primer recurso **/ping** en fichero **init**.py

.. code-block:: python

   # project/__init__.py


   from flask import Flask, jsonify
   from flask_restplus import Resource, Api


   # instantiate the app
   app = Flask(__name__)

   api = Api(app)


   class Ping(Resource):
       def get(self):
           return {
               'status': 'success',
               'message': 'pong!'
           }


   api.add_resource(Ping, '/ping')

Y despues el arranque via cli() creando **manage.py**

.. code-block:: python

   # manage.py

   from flask.cli import FlaskGroup

   from project import app

   cli = FlaskGroup(app)

   if __name__ == '__main__':
       cli()

Y para ejecutar 

.. code-block:: bash

   export FLASK_APP=project/__init__.py
   python manage.py run -host 0.0.0.0

Si quisieramos ejecutar sin cli cerariamos el siguiente fichero **main.py**

.. code-block:: python

   from project import app
   if __name__ == '__main__':
       app.run(host='0.0.0.0')

Y para ejecutar 

.. code-block:: bash

   export FLASK_APP=project/__init__.py
   python main.py

Y para validar navegar a http://10.100.199.200:5000/ping

.. code-block:: json

   {"message": "pong!", "status": "success"}

Una vez tenemos la aplicacion minima desarrollada se crea el fichero de configuracion. Mediante este fichero se gestionara las diferentes configuraciones de la aplicacion en ñas diferentes fases de desarrollo.

.. code-block:: python


   # project/config.py

   class BaseConfig:
       TESTING = False

   class DevelopmentConfig(BaseConfig):
       pass

   class TestingConfig(BaseConfig):
       TESTING = True

   class ProductionConfig(BaseConfig):
       pass

Y ahora modificamos el fichero **_init**.py* para que contemple el fichero de configuración. Para ello se agrega la siguiente linea de codigo.

.. code-block:: python

   app.config.from_object('project.config.DevelopmentConfig') # new

Y ahora ejecutamos Flask en modo Debug mediante la variable de entorno FLASK_ENV, de esta manera consiguimos reloading automático.

.. code-block:: bash

   (env) export FLASK_APP=project/__init__.py
   (env) export FLASK_ENV=development
   (env) python main.py
   python main.py
    * Serving Flask app "project" (lazy loading)
    * Environment: development
    * Debug mode: on
    * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
    * Restarting with stat
    * Debugger is active!
    * Debugger PIN: 244-979-948

Ahora finalizamos el servidor y deactivamos el entorno virtual.
Finalmente creamos el fichero de dependencias **requirements.txt** con la siguiente informacion

.. code-block::

   Flask==1.1.1
   Flask-RESTPlus==0.13.0
   # esto incluir por BUG de Flask-restPlus
   Werkzeug==0.16.1
   importlib-metadata==1.0.0

Ahora ya tenemos la version inicial de de esta practica. Antes de crear y actualizar el repo crear el fichero *.gitignore* con lo siguiente.

.. code-block::

   __pycache__
   env

Y ahora crear el repo en gitlab y actualizarlo. Primero crear el repo para la práctica en `gitlab.danz.eus <https://gitlab.danz.eus>`_.

.. code-block::

   cd flask-tdd-docker/
   git init
   git remote add origin https://gitlab.danz.eus/jaagirre/practica_ci_tdd.git
   git add .
   git commit -m "Initial commit"
   [master (root-commit) 2e599d3] Commit inicial
    8 files changed, 233 insertions(+)
    create mode 100644 .gitignore
    create mode 100644 README.md
    create mode 100644 doc/GIDA_00_HASIERAKETA.md
    create mode 100644 main.py
    create mode 100644 manage.py
    create mode 100644 project/__init__.py
    create mode 100644 project/config.py
    create mode 100644 requirements.txt
   git push origin master

Y ya estamos preparados para trabajar!!
El siguiente paso es Dockerizar la aplicación `Dockerizando <doc/GIDA_01_DOCKERiZAR.md>`_
