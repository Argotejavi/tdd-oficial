
Dockerizando la aplicacion
==========================

En este apartado se dockerizara la base de la aplicación FLASK. De momento se utilizará docker y docker-compose. Si no se dispone de estas aplicaciones instalarlas.

Primero creamos el dockerfile para que cree una imagen con la aplicación.

.. code-block:: Dockerfile

   FROM python:3.8.0-alpine

   RUN mkdir -p /usr/src/app
   WORKDIR /usr/src/app

   ENV PYTHONDONTWRITEBYTECODE 1
   ENV PYTHONUNBUFFERED 1

   COPY ./requirements.txt .
   RUN pip install -r requirements.txt

   # Aqui igual es mejor hacer add en la fase de deasrrollo
   COPY . .

   CMD python manage.py run -h 0.0.0.0

Para que al copiar la aplicación al contenedor no copiar todos los ficheros se crea un fichero .dockerignore.

.. code-block:: .dockerignore

   env
   .dockerignore
   Dockerfile
   Dockerfile.prod

Para gestionar el arranque y el build y minimizar los comandos se creará el Docker-compose. Con el objetivo de no tener que hacer un build cada vez que se cambia el código en la fase de desarrollo se creara un Volumen  relacionado con la carpeta que alberga el codigo de la aplicación, de esta manera no habra que hacer build al probar cada cambio de código. 

.. code-block:: yaml

   version: '3.3'

   services:
       users:
           build:
               context: .
               dockerfile: Dockerfile
           volumes:
               - .:/usr/src/app # Necesaio para la fase de desarrollo
           ports:
               - 5001:5000
           environment:
               - FLASK_APP=project/__init__.py
               - FLASK_ENV=development

Una vez tenemos los ficheros preparados para crear y ejecutar el contenedor ejecutamos los comandos necesarios.

.. code-block:: bash

   docker-compose build
   docker-comnpose up -d

Para probar realizar una petición desde el navegador o sino un curl a la URL http://10.100.199.200:5001/ping y obtendreis

.. code-block:: json

   {
       "status": "success1",
       "message": "pong!"
   }

Una vez tenemos en marcha la aplicacion contenedorizada agregaremos a las variables de entorno la configuraion del enotrno de desarrollo  para diferenciar las diferentes fases del desarrollo. Para esto agragrameos la siguiente linea al **docker-compose.yaml**.

.. code-block:: yaml

     - APP_SETTINGS=project.config.DevelopmentConfig

Finalmente modificamos el **init**.py para obtener la dierccoin de la confdiguracion desde la varibale de entorno especificadas en el Dockerfile.

.. code-block:: python

   import os

   app_settings = os.getenv('APP_SETTINGS')
   app.config.from_object(app_settings)

Y ahora volvemos a ejecutar  el contenedor haciendo build si es necesario

.. code-block:: bash

   docker-compose up -d --build

Y comprobamos que  el servicio ping sigue correctamente. En el caso que se quiera comprobar que se ha cargado la correcta configuración se puede agragar un print al arrancar el sistema en el fichero **init**.py .

.. code-block:: python

   import sys
   print (app.config , file=sys.stderr)

Y ahora si ejecutamos y vemos los logs del contenedor deberias ver algo semejante a lo siguiente

.. code-block:: bash

   docker-compose up -d --build
   eval `ID=$(docker ps --filter="name=users"  -q)`
   docker ps $ID
   <Config {'ENV': 'development', 'DEBUG': True, 'TESTING': False, 'PROPAGATE_EXCEPTIONS': None, 'PRESERVE_CONTEXT_ON_EXCEPTION': None, 'SECRET_KEY': None, 'PERMANENT_SESSION_LIFETIME': datetime.timedelta(days=31), 'USE_X_SENDFILE': False, 'SERVER_NAME': None, 'APPLICATION_ROOT': '/', 'SESSION_COOKIE_NAME': 'session', 'SESSION_COOKIE_DOMAIN': None, 'SESSION_COOKIE_PATH': None, 'SESSION_COOKIE_HTTPONLY': True, 'SESSION_COOKIE_SECURE': False, 'SESSION_COOKIE_SAMESITE': None, 'SESSION_REFRESH_EACH_REQUEST': True, 'MAX_CONTENT_LENGTH': None, 'SEND_FILE_MAX_AGE_DEFAULT': datetime.timedelta(seconds=43200), 'TRAP_BAD_REQUEST_ERRORS': None, 'TRAP_HTTP_EXCEPTIONS': False, 'EXPLAIN_TEMPLATE_LOADING': False, 'PREFERRED_URL_SCHEME':
