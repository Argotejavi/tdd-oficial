
PostgreSQL y SQLAlchemy
=======================

En este apartdao vamos a agregar la base de datos a nuestro servicio WEB y lo relacionaremos con el servicio users.

El primer paso es modificar el fichero requirements.txt para agregar las librerias de acceso a bbdd: Flask-SQLAlchemy y psycopg2. El primero es el ORM de python para Flask y el segundo es la librería de postgress para python que utiliza  ALCHEMY. Esta última librería en ALPINE tiene ciertas dependencias para ser instalada (gcc , pyhton-dev, musl-dev postgresql-dev)

.. code-block:: bash

   Flask-SQLAlchemy==2.4.1
   psycopg2-binary==2.8.4

El siguiente paso es indicar en el fichero  configuración cual es la base de datos a utilizar en cada una de las fases de desarrollo.

.. code-block:: python

   # project/config.py 
   import os  # new

   class BaseConfig:
       TESTING = False
       SQLALCHEMY_TRACK_MODIFICATIONS = False  # new


   class DevelopmentConfig(BaseConfig):
       SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')  # new


   class TestingConfig(BaseConfig):
       TESTING = True
       SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_TEST_URL')  # new


   class ProductionConfig(BaseConfig):
       SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')  # new

Ahora actualizamos para crear una nueva instancia de SQLAlchemy y definir el modelo de la base de datos.

.. code-block:: python

   from flask_sqlalchemy import SQLAlchemy  # new

   # instantiate the db
   db = SQLAlchemy(app)  # new

   class User(db.model):
       __tablename__ = 'users'
       id = db.Column(db.Integer , primary_key=True, autoincrement=True)
       username = db.Column(db.String(128), nullable=False)
       email =  db.Column(db.String(128), nullable=False)
       active =  db.Column(db.Boolean(), default=True, nullable=False)

       def __init__(self, username, email):
           self.username = username
           self.email = email

Ahora creamos una carpeta denominada **db** a la carpeta **project** y creamos un script sql llamado *create.sql* mediante el cual crearemos la base de datos de testeo y la de desarrollo. Además de esto también agregaremos un Dockerfile en la misma carpeta donde definiremos el contenedor de la base de datos.

.. code-block:: sql

   CREATE DATABASE users_dev;
   CREATE DATABASE users_test;

.. code-block:: Dockerfile

   FROM postgres:12.1-alpine

   ADD create.sql /docker-entrypoint-initdb.d

Y finalmente actualizamos el **Docker-compose.yml** para agregar la base de datos

.. code-block:: yaml

   version: '3.3'

   services:

     users:
       build:
         context: .
         dockerfile: Dockerfile
       volumes:
         - '.:/usr/src/app'
       ports:
         - 5001:5000
       environment:
         - FLASK_APP=project/__init__.py
         - FLASK_ENV=development
         - APP_SETTINGS=project.config.DevelopmentConfig
         - DATABASE_URL=postgresql://postgres:postgres@users-db:5432/users_dev  # new
         - DATABASE_TEST_URL=postgresql://postgres:postgres@users-db:5432/users_test  # new
       depends_on:  # new
         - users-db

     users-db:  # new
       build:
         context: ./project/db
         dockerfile: Dockerfile
       expose:
         - 5432
       environment:
         - POSTGRES_USER=postgres
         - POSTGRES_PASSWORD=postgres

Como queremos que el servicio users de FLASK  espere el arranque de la base de datos se crea un script Bash (entrypoint.sh, ver el fichero en el repositoro) que espera el arranque de la base de datos antes de arrancar el srevicio FLASK. Para elllo ademas de crear el script de arranque con la espera tambien modificaremso el dockerfile para que ejecuta el script de espera e instale las dependencias  necesarias en este caso netact-openbsd para el comando nc . También modificamos para que el entrypoint se ejecute desde el compose

.. code-block:: Dockerfile

   # pull official base image
   FROM python:3.8.0-alpine

   # new
   # install dependencies
   RUN apk update && \
       apk add --virtual build-deps gcc python-dev musl-dev && \
       apk add postgresql-dev && \
       apk add netcat-openbsd

   # set environment varibles
   ENV PYTHONDONTWRITEBYTECODE 1
   ENV PYTHONUNBUFFERED 1

   # set working directory
   WORKDIR /usr/src/app

   # add and install requirements
   COPY ./requirements.txt /usr/src/app/requirements.txt
   RUN pip install -r requirements.txt

   # new
   # add entrypoint.sh
   COPY ./entrypoint.sh /usr/src/app/entrypoint.sh
   RUN chmod +x /usr/src/app/entrypoint.sh

   # add app
   COPY . /usr/src/app

Finalmente hacemos el docker-compose up -d --build y probamos que todo sigue igual.

Finalmente modificamos el manage.py para que cree un nuevo comando para que podamos ejecutarlo desde el CLI, en este caso el comando permite crear un nuevo  de base de datos facilmente.

.. code-block:: python

   # new
   @cli.command('recreate_db')
   def recreate_db():
       db.drop_all()
       db.create_all()
       db.session.commit()
