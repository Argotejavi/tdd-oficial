# TDD and Continuous integration  with docker, python flask, gitlab and heroku: PYTEST

A continuación vamos empezar a utilizar el framework de testeo PyTest.  Antes de comenzar crear la carpeta *test* donde se incluirán la configuración de testeo, el fichero principal del modulo,  junto con otros ficheros. Por defecto PyTest ejecuta todos los ficheros que empiezan o terminar por *test*. las funciones de test deben de comenzar con **_test** y si se quieren utilizar clases estas deben de empezar con la palabra **Test**-

Par instalar pytest simplemente se debe de instalar con pip la libreria pytest. Por lo tanto modificar el fichero requirements.txt de la siguiente forma:


```bash
pytest==5.3.1
```

Una vez agregada la libreráa a las dependencias, el siguiente paso es crear los fixtures (setup y teardown) para los test. Estos los crearemos en el fichero *project/tests/conftest.py*. Recordad que mediante el comando de return **yield** de python se consigue parar la función y devolver un valor para en la siguiente llamada volver a seguir en el mismo punto.


```python
import pytest
from project import app , db

@pytest.fixture(scope='module')
def test_app():
    app.config.from_object('project.config.TestingConfig')
    with app.app_context():
        yield app 

@pytest.fixture(scope='module')
def test_database():
    db.create_all()
    yield db
    db.session.remove()
    db.drop_all()

```

Y ahora podemos probar a crear los contenedores y ejecutar los test.

```bash
docker-compose up -d --build 
docker-compose exec users python -m pytest "project/tests"
```

Una vez probado que pytest está correctamente instalado en la aplicación pasamos a crear los primeros test de la aplicación. Y de momento vamos a testear las diferentes configuraciones de la aplicación: testing, development y production

```python
# project/test/test_config.py

import os

def test_development_config(test_app):
    test_app.config.from_object('project.config.DevelopmentConfig')
    assert test_app.config['SECRET_KEY'] == 'my_precious'
    assert not test_app.config['TESTING']
    assert test_app.config['SQLALCHEMY_DATABASE_URI'] == os.environ.get('DATABASE_URL')

def test_testing_config(test_app):
    test_app.config.from_object('project.config.TestingConfig')
    assert test_app.config['SECRET_KEY'] == 'my_precious'
    assert test_app.config['TESTING']
    assert not test_app.config['PRESERVE_CONTEXT_ON_EXCEPTION']
    assert test_app.config['SQLALCHEMY_DATABASE_URI'] == os.environ.get('DATABASE_TEST_URL')

def test_production_config(test_app):
    test_app.config.from_object('project.config.ProductionConfig')
    assert test_app.config['SECRET_KEY'] == 'my_precious'
    assert not test_app.config['TESTING']
    assert test_app.config['SQLALCHEMY_DATABASE_URI'] == os.environ.get('DATABASE_URL')

```

Ahora ya podemos ejecutar el nuevo código sin tener que volver a acrear el contenedor, por que estamos trabajando con un volumen. Y como estamos utilizando el app con diferentes configuraciones en un mismo test suite.

Al ejecutar el test obtendremos lo siguiente

```bash
docker-compose exec users python -m pytest 'project/test' 
______________________________________________ test_production_config _______________________________________________

test_app = <Flask 'project'>

    def test_production_config(test_app):
        test_app.config.from_object('project.config.ProductionConfig')
>       assert test_app.config['SECRET_KEY'] == 'my_precious'
E       AssertionError: assert None == 'my_precious'

project/test/test_config.py:20: AssertionError
================================================= 3 failed in 0.39s ===================
```

Para solventar el error modificamos la configuración de test y volvemos a ejecutar.

```python
class BaseConfig:
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'my_precious'  # new
```
Y ahora ya obtenemos que los tres test pasan correctamente

```bash
collected 3 items

project/test/test_config.py ...                                                                               [100%]

================================================= 3 passed in 0.15s ==================
```

Ahora realziaremos un test funcional sobre el servicio **Ping**

```python
# project/test/test_ping.py
import json

def test_ping(test_app):
    client = test_app.test_client()
    resp = client.get('/ping')
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert 'pong' in data['message']
    assert 'success1' in data['status']


```
La salida ha indicado que esperaba el status  **success1** y ha recibido **success**. Para que el test de bien , modificar el assert o el API ping ;-)

Si ejecutamos tendremos la siguiente salida 

```
project/test/test_config.py ...                                                                                                                                  [ 75%] project/test/test_ping.py .                                                                                                                                      [100%]

========================================================================== 4 passed in 0.20s =================
```

Este último test se podría catalogar como funcional mientras que los anteriorres podrian set unitarios. Si queremos se puede diferenciar en dos carpetas dentro de test : unit/ y /functional

Y para terminar veremos como podemos seleccionar los test a ejecutar. Por ejemplo podemos filtrar por nombres de test. Como ejemplo solo ejecutaremos los test de configuración filtrando por config.


```bash
docker-compose exec users python -m pytest 'project/tests' -k config
collected 4 items / 1 deselected / 3 selected

project/test/unit/test_config.py ...                                                                                                                             [100%]

=================================================================== 3 passed, 1 deselected in 0.23s ==========
```




