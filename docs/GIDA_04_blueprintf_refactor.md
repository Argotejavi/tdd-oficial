# TDD and Continuous integration  with docker, python flask, gitlab and heroku: FLASK BLUEPRINTS

En este apartado vamos a reestructurar el sw de la aplicación mediante el uso de FLASK BLUEPRINTS.


Primero creamos la carpeta *api* y creamos en la  nueva carpeta el fichero de modulo **\_\_init\_\_.py** junto con el fichero de *ping.py*, *users.py* y *models.py*. Y ahora moveremos a estos ficheros parte del código que teniamos en el fichero **project/__init__.py**.


Empezamos con el fichero *ping.py*, donde se establece el API  que responde a la url */ping*. 


```python
from flask import Blueprint
from flask_restplus import Resource, Api


ping_blueprint= Blueprint('ping', __name__)
api = Api(ping_blueprint)

class Ping(Resource):
    def get(self):
        return{
            'status': 'success',
            'message': 'pong!'
        }

api.add_resource(Ping, '/ping')
```

No olvidar quitar este código de **project/__init__.py**.

Una vez creado el Blueprint con el API y el recurso se crea el modelo de base de datos. Esto lo haremos en **project/api/models.py**. No olvideis de quitar el codigo de la Clase User del fichero **project/__init__.py**.

```python
from sqlalchemy.sql import func
from project import db

class User(db.Model):
    __tablename__='users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)
    created_data = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, username, email):
        self.username = username
        self.email = email
```

Ahora  modificamos el fichero *project/__init__.py* como lo siguiente.

```python
import os 
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()

def create_app(script_info=None):
    app = Flask(__name__)

    app_settings = os.getenv('APP_SETTINGS') # incluye direccion fichero
    app.config.from_object(app_settings)

    db.init_app(app)


    from project.api.ping import ping_blueprint

    app.register_blueprint(ping_blueprint)

    @app.shell_context_processor
    def ctx():
        return {'app':app, 'db':db}

    return app
```

Ahora modificamos el fichero *manage.py* para que utilice la funcion *create_app()* e incorpore las siguientes lineas. La linea que tenia la creacion de la variable cli anterior sustituirla por la nueva. Y quitar la siguiente linea **from project import app** ya que ahora utilizamos nuestra función **create_app()** para obtener la instancia de Flask.

```python

from project import create_app, db
from project.api.models import User # 

app = create_app()
cli = FlaskGroup(create_app = create_app)


```

Una vez en este punto podemos probar a lanzar los contenedores y lanzar la shell que crea una aplicación de Flask donde podemos ver el contexto de: *app* y *db*.

```bash
docker-compose up -d --build
docker-compose exec users flask shell

>>>>app
<Flask, 'Project'>
>>>> db
<SQLAlchemy engine=postgresql://postgres:***@users-db:5432/users_test>
>>> exit()
```
Ahora en los testeos podemos crear tantas instancias de aplicación como queremos mediante el factory *create_app*. Para ello vamos a modificar el fichero *conftest.py* de los testeos.

```python
import pytest
from project import create_app, db

@pytest.fixture(scope='module')
def test_app():
    app = create_app()
    app.config.from_object('project.config.TestingConfig')
    with app.app_context():
        yield app

@pytest.fixture(scope='module')
def test_database():
    db.create_all()
    yield db
    db.session.remove()
    db.drop_all()

```

Como ahora utilizamos *create_app()* en los test no es necesario definir FLASK_APP  para cuando queremos obtener la aplicacion de Flask *app*.  Ahora para testear arrancamos los contenedores y ejecutamos los tests. Ejecutamos  los test

```bash
docker-compose exec users python -m pytest "project/tests"
-- Docs: https://docs.pytest.org/en/latest/warnings.html
=========================================================== 4 passed, 2 warnings in 0.40s ================
```

Y ahora podemos ejecutar el comando shell *recreate_db*. Y luego nos conectamos a la base de datos postgress para verificar lo ocurrido.
```bash
docker-compose exec users python manage.py recreate_db
docker-compose exec users-db psql -U postgres
postgres=# \c users_dev
You are now connected to database "users_dev" as user "postgres".
users_dev=# \dt
         List of relations
 Schema | Name  | Type  |  Owner
--------+-------+-------+----------
 public | users | table | postgres
(1 row)
users_dev=# \q

```

Acordaros que el fichero **manage.py** debe de tener el siguient código:

```python
# new
@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()

```

Podemos apreciar que ahora tenemos una funcion de cli/bash para borrar la base de datos ;-)


