
Flask Blueprints
================

En este apartado vamos a reestructurar el sw de la aplicación sw mediante el uso de FLASK BLUEPRINTS.

Primero creamos la caprtea *api* y añadimos el fichero de modulo **_init**.py\ * junto con el fichero de *\ ping.py\ *, *\ users.py\ * y *\ models.py*. 

Empezamos con el fichero *ping.py*\ , donde se establece el API  que responde a la url */ping*. 

.. code-block:: python

   from flask import Blueprint
   from flask_restplus import Resource, Api


   ping_blueprint= Blueprint('ping', __name__);
   api = Api(ping_blueprint)

   class Ping(Resource):
       def get(self):
           return{
               'status': 'success',
               'message': 'pong!'
           }

   api.add_resource(Ping, '/ping')

Una vez creado el Blueprint con el API y el recurso se crea el modelo de base de datos.

.. code-block:: python

   from sqlalchemy.sql import func
   from project import db

   class User(db.Model):
       __tablename__='users'
       id = db.Column(db.Integer, primary_key=True, autoincrement=True)
       username = db.Column(db.String(128), nullable=False)
       active =db.Column(db.String(128), nullable=False)
       created_date = db.Column(db.DateTime, default=fucn.now(), nullable=False)

       def __init__(self,username , email):
           self.username = username
           self.email = email

Una vez creado el modelo de usuario pasamos al fichero prinicipal de la carpeta API, su fichero **_init**.py*.

.. code-block:: python

   import os 
   from Flask import Flask
   from flask_alchemy import SQLAlchemy


   db = SQLAlchemy()

   def create_app(script_info=None):
       app = Flask(__name__)

       app_setting = os.getenv('APP_SETTINGS') # incluye direccion fichero
       app.config.from_object(app_settings)

       db.init_app(app)


       from project.api.ping import ping_blueprint

       app.register_blueprint(ping_blueprint)

       @app.shell_context_processor
       def ctx():
           return {'app':app, 'db':db}

       return app

Ahora modificamos el fichero *manage.py* para que utilice la funcion *create_app()* e incorpore las siguientes lineas.

.. code-block:: python


   from project import create_app, db
   from project.api.models import User # 

   app = create_app()
   cli = FlaskGroup(create_app = create_app)

Una vez en este punto podemos probar a lanzar los contenedores y lanzar la shell que crea una aplicación de Flask donde podemos ver el contexto de: *app* y *db*.

.. code-block:: bash

   docker-compose build -d --build
   docker-compose exec users flask shell

   >>>>app
   <Flask, 'Project'>
   >>>> db
   <SQLAlchemy engine=postgresql://postgres:***@users-db:5432/users_test>
   >>> exit

Ahora en los testeos podemos crear tantas instancias de aplicación como queremos mediante el factory *create_app*. Para ello vamos a modificar el fichero *conftest.py* de los testeos.

.. code-block:: python

   import pytest
   from project import create_app, db

   @pytext.fixture(scope='module')
   def test_app():
       app = create_app()
       app.config.from_object('project.config.TestingConfig')
       with app.app_context():
           yeld app

   @pytest.fixture(scope='module')
   def test_database():
       db.create_all()
       yeld db
       db.session.remove()
       db.drop_all()

Como ahora utilizamos *create_app()* en los test no es necesario definir FLASK_APP  para cuando queremos obtener la aplicacion de Flask *app*.  Ahora para testear arrancamos los contenedores y ejecutamos los tests. Ejecutamos  los test

.. code-block:: bash

   docker-compose exec users python -m pytest "project/test"
   -- Docs: https://docs.pytest.org/en/latest/warnings.html
   =========================================================== 4 passed, 2 warnings in 0.40s ================

Y ahora podemos ejecutar el comando shell *recreate_db*. Y luego nos conectamos a la base de datos postgress para verificar lo ocurrido.

.. code-block:: bash

   docker-compose exec users python manage.py recreate_db
   docker-compose exec users-db psql -U postgres
   postgres=# \c users_dev
   You are now connected to database "users_dev" as user "postgres".
   users_dev=# \dt
            List of relations
    Schema | Name  | Type  |  Owner
   --------+-------+-------+----------
    public | users | table | postgres
   (1 row)
