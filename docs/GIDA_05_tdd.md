# TDD and Continuous integration  with docker, python flask, gitlab and heroku: TDD 

Este paso vamos agregar las siguiente rutas al API restful utilizando una metodología TDD.

|Endpoint | HTTP Method | CRUD Method | Result |
|---|---|---|---|
| /users | GET | READ | get all users |
| /users/:id | GET | READ | get a user |
| /users | POST | CREATE | ADD a user |

Para cada una de las rutas:

1. Escribiremos el Test
2. Ejecutaremos el Test y fallará (ESTADO ROJO)
3. Escribiremos el mínimo código posible para pasar el test(ESTADO VERDE)
4. Refcatorizaremos el código si se ve necesario


Primero escribiremos el test para el POST */user* en el fichero *project/test/test_user.py*

```python
# project/tests/test_users.py

import json 

def test_add_user(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        '/users',
        data = json.dumps({
            'username': 'michael',
            'email' : 'michael@mail.com'
        }),
        content_type='application/json'
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert 'michael@mail.com was added!' in data['message']

```
Si ahora ejecutamos el test obtendremos un error al decodificar como json la respuesta HTML del servidor Flask (flask esta indicando con un página HTML que no existe la ruta). Esto ocurre porque aun no hemos implementado la ruta del API */users*.

```bash
docker-compose exec users python -m pytest 'project/tests'
___________________________________________________________________ test_add_user ________________________________________________________________
data = json.loads(resp.data.decode())

project/test/test_users.py:14:
====================================================== 1 failed, 4 passed,
```

Si no supiéramos la razón de este error deberíamos debugear el Test para ver concretamente la razón del error, y ver así si es error de la escritura del test o del sw. Normalmente el debugeo lo realizamos en el Software under Test (SUT) , pero en este caso aún no hemos implementado el SUT y queremos saber si es fallo del código de test o es debido a que no está implementada la ruta API.

Para poder debugear código alojado en un contenedor se utiliza el **Debugging Remoto**. Para poder realizar debugging remoto en aplicaciones *Python*  *Visual Code* ofrece el modulo Python **ptvsd**. Incrustando este modulo en el software remoto a debugear visual code conseguirá comunicarse y permitirá debugear el código. Como ejemplo incrustaremos el servidor *ptvsd* de dos maneras: 1) En el código de Testeo y 2) En el SUT, el servidor flask en este caso.

Primero crearemos una configuración de debugging remoto en Visual Code. Esto se realiza en el fichero **.vscode/launch.json**. Este fichero establecerá la conexión remoto hacia la IP del host definido y se debe de indicar donde se aloja local y remotamente el código a debugear. En este caso el contenedor se ejecuta en la máquina 10.100.199.200 , en el puerto 14001 y la carpeta del código remoto en el contenedor es */usr/src/app*.

Debido a que vosotros estais utilizando pycharm. L aparte del debugging , es decir el siguiente paso , os lo podeis saltar. En breve os pondremos una pildora extra sobre el debugging remoto desde pycharm.
Ahora saltar hasta el [siguiente paso](#step)

```json
{
    "version": "0.2.0",
    "configurations": [
      
        {
            "name": "Attach (Remote Debug)",
            "type": "python",
            "request": "attach",
            "port": 14001,
            "host": "10.100.199.200",
            "pathMappings": [{
                "localRoot": "${workspaceFolder}/",
                "remoteRoot": "/usr/src/app"
            }]
        },
    ]
}
```
El  siguiente paso es agregar el servidor *ptvsd* en el código de Test. Esta práctica no es habitual ya que normalmente se debugea el SUT y no el TEST. Para no pervertir con esta dependencia el código del Test se crea en el docker-compose una variable de entorno que nos permitirá decidir si queremos debugging remoto o no (REMOTE_DEBUG), de esta forma la automatización CI no se verá perjudicado. En el siguiente código incrustamos en  el servidor *ptvsd* en el test mediante un fixture (setup) de nivel de sesión  y a continuación configuramos la variable de entorno. Además de esa variable de entorno, hay que exponer el puerto (14001) y poner a false la variable FLASK_DEBUG (se desactiva el debug del servidor Flask para que permita el debugeo remoto).

```python
# project/test/conftest.py

import ptvsd

@pytest.fixture(scope="session", autouse=True)
def test_debug():
    test_type = os.getenv('REMOTE_DEBUG')
    if test_type == "True":
        import ptvsd
        ptvsd.enable_attach( address = ('0.0.0.0', 14001) )
        ptvsd.wait_for_attach()
```

```yaml
#docker.compose.yml
    ports:
      - 5001:5000
      - 14001:14001
    environment:
      - REMOTE_DEBUG=True
      - FLASK_DEBUG=False
```

Ahora si creamos nuevamente la imagen del contenedor y ejecutamos los testeos veremos como se bloquea la ejecución del test en el contenedor. En ese momento desde visual Code ejecutaremos la configuración de Debugging y veremos como podemos establecer breakpoint y debugear el propio test. Si establecemos el breakpoint en la línea *** data = json.loads(resp.data.decode())** del  fichero **test_users.py** veremos que **resp.data** no tiene formato json y de ahí el error. En otra situaciones, como los test de integración queremos debugear el código del servidor Flask , es decir el SUT. En esos casos podemos cambiar el entrypoint del contenedor **users** para ejecutar Flask junto con un servidor **ptvsd**, ver el siguiente código:

```bash
python -m ptvsd --host 0.0.0.0 --port 14001 --wait manage.py run -h 0.0.0.0
```
La necesidad del Debugging es fundamental en el proceso de desarrollo, pero no es necesario en el pipeline de CI. Para poder gestionar correctamente los diferentes entrypoint y variables de entorno tendremos que utilizar **overrides** de docker-compose o dockerfiles multistage. Esta problemática nos aparecerá mas adelante. De momento sigamos desarrollando. 

<a id="step"></a>
Una vez tenemos escrito el test y estamos en estado **ROJO** el siguiente paso en el proceso **TDD** es implementar el SUT. En nuestro caso lo haremos creando un  Blueprint  de Flask como hicimos para */ping* con */users*. Crearemos le fichero *api/users.py* implementado la función POST. Una vez creado el Blueprint este habrá que registrarlo al crear la aplicación FLASK en la funcion *create_app()* de fichero *project/__init__py*.


```python
# project/api/users.py
from flask import Blueprint, request
from flask_restplus import Resource, Api

from project import db
from project.api.models import User


users_blueprint = Blueprint('users', __name__)
api = Api(users_blueprint)

class UsersList(Resource):
    def post(self):
        post_data = request.get_json()
        username = post_data.get('username')
        email = post_data.get('email')
        db.session.add(User(username=username, email=email))
        db.session.commit()
        response_object = {
            'message': f'{email} was added!'
        }
        return response_object, 201 
        
api.add_resource(UsersList, '/users')

```
```python
# project/__init__.py

 from project.api.users import users_blueprint
    app.register_blueprint(users_blueprint)

```
Si ahora volvemos a ejecutar los test veremos como todo los test se pasan satisfactoriamente.

```bash
docker-compose exec users python -m pytest 'project/tests'
============================ 5 passed, 2 warnings in 16.19s =
```

Para evitar la parada del servidor *ptvsd* de momento volver a modificar el *docker-compose.yml* y poner a False el *REMOTE_DEBUG*, y para que tenga efecto volver a crear el contenedor. Si para la ejecución de los test no queremos ver warnings agregar a pytest -p no:warnings.

```bash
docker-compose exec users python -m pytest 'project/test' -p no:warnings
================================================================ test session starts =================================================================platform linux -- Python 3.8.0, pytest-5.3.1, py-1.8.1, pluggy-0.13.1
rootdir: /usr/src/app
collected 5 items

project/test/test_users.py .                                                                                                                   [ 20%] project/test/functional/test_ping.py .                                                                                                         [ 40%] project/test/unit/test_config.py ...                                                                                                           [100%]

================================================================= 5 passed in 1.05s ============
```

Una vez ejecutado el test p el fixture elimina todos los datos creados por ello no veréis datos en la base de datos.

```bash
docker-compose exec users-db psql -U postgres
\c users_test
\dt
```

Si paramos mediante un breakpoint  antes de ejecutar el fixture del test  podremos ver como se inserta en la base de datos de test el usuario.
```bash
users_test=# SELECT * FROM users;
 id | username |      email       | active |        created_data
----+----------+------------------+--------+----------------------------
  1 | michael  | michael@mail.com | t      | 2020-01-21 16:16:45.614771
(1 row)

users_test=# SELECT * FROM users;
ERROR:  relation "users" does not exist
LINE 1: SELECT * FROM users;
                      ^
```

A continuación seguimos implementando mas funcionalidades y rutas. Primero vamos a agregar control de errores y excepciones a la funcionalidad de agregar usuario: Las siguientes funcionalidades se agregaran vía TDD

- JSON invalidado al agregar usuario
- Valores incorrectos en el JSON al agregar un usuario
- Agregar un usuario ya existente

A continuación mediante los test se especifican los 3 requisitos anteriores:

```python
# project/test/
def test_add_user_invalid_json(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        '/users',
        data = json.dumps({}),
        content_type='application/json'
    )
    data = json.loads( resp.data.decode() )
    assert resp.status_code == 400
    assert 'Input payload validation failed' in data['message']

def test_add_user_invalid_json_keys(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        '/users',
        data = json.dumps({"email": "john@gmail.com"}),
        content_type = 'application/json'
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert 'Input payload validation failed' in data['message']

def test_add_user_duplicate_email(test_app, test_database):
    client = test_app.test_client()
    client.post(
        '/users',
        data = json.dumps({
            'username':'michael',
            'email' : 'michael@gmail.com'
        }),
        content_type='application/json'
    )
    resp = client.post(
        '/users',
        data = json.dumps({
            'username' : 'michael',
            'email' : 'michael@gmail.com'
        }),
        content_type='application/json'
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert 'Sorry. That email alreadey exists' in data['message']
```

Una vez fallan estos test el siguiente paso es implementar el SUT correctamente para responder a estos requisitos.


```python 
from sqlalchemy import exc

class UsersList(Resource):
    def post(self):
        post_data = request.get_json()
        username = post_data.get('username')
        email = post_data.get('email')
        response_object = {}

        user = User.query.filter_by(email=email).first()
        if user:
            response_object['message'] = 'Sorry. That email alreadey exists'
            return response_object, 400
        try:
            db.session.add(User(username=username, email=email))
            db.session.commit()
            response_object['message'] = f'{email} was added!'
            return response_object, 201 
        except:
            db.session.rollback()
            response_object['message'] = f'Input payload validation failed' 
            return response_object, 400
```

Y si ahora ejecutamos obtenemos  los test obtendremos que los 8 test se pasan correctamente.

```bash
docker-compose exec users python -m pytest 'project/tests'
=================== test session starts =============================
platform linux -- Python 3.8.0, pytest-5.3.1, py-1.8.1, pluggy-0.13.1
rootdir: /usr/src/app
collected 8 items

project/test/test_users.py ....                          [ 50%] project/test/functional/test_ping.py .                                [ 62%] project/test/unit/test_config.py ..                                        [100%]==================================== 8 passed in 1.18s =================
```


El modulo de Flask RestPlus  ofrece un validador de modelo de datos, que al utilizarlo nos puede evitar escribir código de validación y no basarnos únicamente en el tratamiento de excepciones,  algo peligroso, o sino podríamos escribir un modulo de Validación. Para utilizar este validador utilizaremos un el decorador   **@api.expect(user, validate=True)** en la función que se requiera, en nuestro caso de momento en el POST. Aquí tenemos el código que se debe de agregar para uso de modelos de validación de RestPlus.

```python
# project/api/users.py

from flask_restplus import Resource, Api, fields

user = api.model('User', {
    'id': fields.Integer(readOnly=True),
    'username': fields.String(required=True),
    'email': fields.String(required=True),
    'created_date': fields.DateTime,
})

class UsersList(Resource):

    @api.expect(user, validate=True) # !!!!!!!!
    def post(self):

```


Una vez tenemos implementado la ruta **POST /users** pasamos a implementar las demás rutas siempre mediante TDD. Comenzarnos escribiendo el TEST de la ruta GET   **/user/:id**.

```python

from project.api.models import User
from project import db

def test_single_user(test_app, test_database):
    user = User(username='jeffrey' , email='jeffrey@gmail.com')
    db.session.add(user)
    db.session.commit()
    client = test_app.test_client()
    resp = client.get(f'/users/{user.id}')
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert 'jeffrey' in data['username']
    assert 'jeffrey@gmail.com' in data['email']

```

 Si ahora ejecutamos los tests obtendremos un error al decodificar el JSON de los datos de respuesta, ya la ruta no esta implementada. 

 ```bash
docker-compose exec python -m pytest 'project/test'
 >       data = json.loads( resp.data.decode())
 55: JSONDecodeError
================ 1 failed, 8 passed in 1.05s =====

project/test/test_users.py:74:
 ```

Por lo tanto volvemos a estar en ROJO y tenemos que implementar la ruta GET. Para ello implementamos un recurso API  TIPO get con la URL */users/:user_id* y agregamos el decorador para que convierta el objeto alchemy del response al JSON definido en el modelo (marshalling, serialziacion).

```python
# project/api/users.py
class Users(Resource):

    @api.marshal_with(user) # este decorador se utiliza para pasar los datso del get a un JSON
    def get(self, user_id):
        return User.query.filter_by(id=user_id).first() , 200




api.add_resource(Users, '/users/<int:user_id>')
```

Si ahora ejecutamos el test nos dará correctamente la información del usuario y el test pasa correctamente. El siguiente paso es agregar requisitos de control de datos al GET de */users*. Para ello especificaremos en un test la respuesta a dar cuando el id del usuario es incorrecto.

```python
def test_single_user_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get('/users/999')
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert 'user 999 does not exist' in data['message']
```

Y para dar respuesta a este test se modifica el GET  en el recurso Users de la siguiente manera.

```python

class Users(Resource):
   @api.marshal_with(user) # este decorador se utiliza para pasar los datso del get a un JSON
    def get(self, user_id):
        user = User.query.filter_by(id=user_id).first()
        if not user: 
            api.abort(404, f'user {user_id} does not exist')
        return  user , 200
```


Una vez el test es correcto pasamos a la siguiente ruta, en este caso,el GET de todos los usuarios. Como para este test se requiere de previamente haber agregado una colección de usuarios se utilizara un FIXTURE de setup para insertar los datos en la base de datos. Este factory también nos valdrá para refactorizar  el test de agregar usuario. Primero vamos con el fixture que ofrece una función par agregar un usuario en **conftest.py**.

```python
from project.api.models import User

@pytest.fixture(scope='function')
def add_user():
    def _add_user(username,email):
        user = User(username=username , email = email)
        db.session.add(user)
        db.session.commit()
        return user
    return _add_user
```

Y ahora refactorizamos primero el test de  ver si existe un usuario y así reducimos dos líneas para cada agregación de usuarios de testeo.


```python
def test_single_user(test_app, test_database, add_user):
    user = add_user('jeffrey', 'jeffrey@testdriven.io')
    client = test_app.test_client()
    resp = client.get(f'/users/{user.id}')
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert 'jeffrey' in data['username']
    assert 'jeffrey@testdriven.io' in data['email']
```

Y una vez validado que la refactorizacion no cambie el resultado de los test implemenetaremos el test para ver una lista de usuarios.

```python 
def test_all_users(test_app, test_database, add_user):
    add_user('urtzi', 'urtzi@gmail.com')
    add_user('joseba', 'joseba@gmail.com')
    client = test_app.test_client()
    resp = client.get('/users')
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert 'urtzi' in data[0]['username']
    assert 'urtzi@gmail' in data[0]['email']
    assert 'joseba' in data[1]['username']
    assert 'joseba@gmail.com' in data[1]['email']

```

Si ahora ejecutamos el test veremos que estamos en estado ROJO , y solo nos queda implementar el recurso API UsersList.

```python 
@api.marshal_with(user, as_list=True)
def get(self):
    return User.query.all(),200
```

Si lanzamos el test ahora veremos como el test nos da error en caso de que ejecutemos todos los test, concretamente nos dice que el numero de usuarios es 4 en lugar de dos. Esto ocurre porque la base de datos tiene un alcance de modulo por lo que ahí otros dos test que insertan usuarios en la base de datos.


```bash
================= FAILURES ================================
______________ test_all_users ___
test_app = <Flask 'project'>, test_database = <SQLAlchemy engine=postgresql://postgres:***@users-db:5432/users_test>
add_user = <function add_user.<locals>._add_user at 0x7fcf6723bc10>

    def test_all_users(test_app, test_database, add_user):
        add_user('urtzi', 'urtzi@gmail.com')
        add_user('joseba', 'joseba@gmail.com')
        client = test_app.test_client()
        resp = client.get('/users')
        data = json.loads(resp.data.decode())
        assert resp.status_code == 200
>       assert len(data) == 2
E       AssertionError: assert 4 == 2
E        +  where 4 = len([{'created_date': None, 'email': 'michael@mail.com', 'id': 1, 'username': 'michael'}, {'created_date': None, 'email': ...om', 'id': 5, 'username': 'urtzi'}, {'created_date': None, 'email': 'joseba@gmail.com', 'id': 6, 'username': 'joseba'}])

project/test/test_users.py:92: AssertionError
========== 1 failed, 10 passed,
```

Para solventar este error  cada de test debería estar aislado de los demás (Por ejemplo, varios desarrolladores cada uno responsable de diferentes test). Para solventar este problema tenemos diferentes estrategia: 1) Manualmente adaptar el código de un test (mala solución) 2) Borrar  la base de datos en cada test que lo requiera 3) Mediante un fixture borrar siempre la base de datos, puede ralentizar la ejecución con alcance de función 4) Crear dos fixture de base de datos  y utilizar el necesario.

En el siguiente código proponemos las soluciones 2 y 3. Y vemos como la solución 2 es más rápida que la  3 pero requiere agregar código en todas aquellas funciones que lo requieran.


```python
@pytest.fixture(scope='function')
def test_database():
    db.create_all()
    yield db
    db.session.remove()
    db.drop_all()
```
```bash
=========== 11 passed, 2 warnings in 1.15s =========
```

```python
 test_database.session.query(User).delete()
```
```bash
============= 11 passed, 2 warnings in 0.86s ======
```

Y ahora vamos a comprobar que la app en ejecución no tiene los datos de los test, para ello ejecutar un curl a la URL de /users.

```bash
curl 10.100.199.200:5001/users
[]
```
Si quisiéramos tener en la base de datos de desarrollo una serie de datos por defecto , una solución es modificar el fichero manage.py para que genere un comando cli para que agregue datos en las bases de datos.

```python
@cli.command('seed_db')
def seed_db():
    db.session.add(User(username='aitor', email="aitor@gmail.com"))
    db.session.add(User(username='gorka', email="gorka@gmail.com"))
    db.session.commit()
```

Y si ahora ejecutamos en el contenedor de desarrollo el cli agregaremos datos a la base de datos que se encuentra en producción.

```bash
 docker-compose exec users python manage.py recreate_db
 docker-compose exec users python manage.py seed_db
 curl 10.100.199.200:5001/users
 [
    {
        "id": 1,
        "username": "aitor",
        "email": "aitor@gmail.com",
        "created_date": null
    },
    {
        "id": 2,
        "username": "gorka",
        "email": "gorka@gmail.com",
        "created_date": null
    }
]
```
También podemos ejecutar los tests y ver que la base de datos de desarrollo no se ve alterada ;-)

Para finalizar resumimos diferentes comando PyTest que permiten seleccionar  test, eliminar outputs, parar los test dependiendo de los resultados, enseñar variables locales.

```bash
# normal run
$ docker-compose exec users pytest "project/tests"

# Filtrando por nombre , el siguiente comando solo ejecutaria uno de los dos siguientes ficheros TestMyClass.test_something, TestMyClass.test_method_simple ¿cual?
$ docker-compose exec users pytest "project/tests" -k "MyClass and not method"

# Para ejecutar un test en un modulo concreto
$ docker-compose exec users pytest test_mod.py::test_func

# disable warnings
$ docker-compose exec users pytest "project/tests" -p no:warnings

# run only the last failed tests
$ docker-compose exec users pytest "project/tests" --lf

# run only the tests with names that match the string expression
$ docker-compose exec users pytest "project/tests" -k "config and not test_development_config"

# stop the test session after the first failure
$ docker-compose exec users pytest "project/tests" -x

# enter PDB after first failure then end the test session
$ docker-compose exec users pytest "project/tests" -x --pdb

# stop the test run after two failures
$ docker-compose exec users pytest "project/tests" --maxfail=2

# show local variables in tracebacks
$ docker-compose exec users pytest "project/tests" -l

# list the 2 slowest tests
$ docker-compose exec users pytest "project/tests"  --durations=2

```











