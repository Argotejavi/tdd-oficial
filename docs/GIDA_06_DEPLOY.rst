
Deploy
======

En este apartado  se desplegara el API desarrollado y testeado en la plataforma Heroko/AppEngine/ElasticBeanStalk. El objetivo de este paso es ver las diferentes necesdades que pueden surgir entre un entorno de desarrollo/Test y el de producción.

La primera diferencia entre el entorno  d edesarrollo y producción de este ejemplo es que el tipo de servidor. El servidor que trae el Framework Flask no es adecudao para el nivel de producción,  por ello la aplicacion se desplegara en el servidor GUNICORN, un servidor WSGI de nivel de producción. 

La primeta tarea que conlleva est erequisito es la modoificacion de las dependencias siendo necesario instalara GUNICORN. Para ello se cambia el fichero *requirements.txt*. Herramientas de dependencias como **npm** permiten diferenciar entre las dependencias a instalar en las diferentes fases de desarrollo. Pip no ofrece esta posibilidad, por lo que en pyhton tendrian varias posibilidades,  la primera utilizra varios fichero de dependencias, una para cada entorno. La segunda utiloizar entornos vrituales mediante pipenv. Como nosostros trabajamos con docker , una posible solucion que nos puede ayudar para configurar el conetxto de cada entorno puede ser utilizar diferentes dockerfiles. 

Primero agregamos al fichero de dependencias *gunicorn*.

.. code-block:: config

   gunicorn==20.0.4

Despeus creamos un nuevo fichero *Dockerfile.prod*  destiando al despliegue. Respecto al entorno de desarrollo las diferencias son :

.. code-block::

   - Variables de entorno ( que indican la fase de desarrollo y configuracion de base de datos).
   - El entrypoint o un CMD
   - Modifcacion de usuario que ejecuta la aplicacion (que no sea root, recomendacion de HEROKU)
   - Variable $PORT utilizada por HEROKU para el despliegue



.. code-block:: Dockerfile

   # pull official base image
   FROM python:3.8.0-alpine

   # install dependencies
   RUN apk update && \
       apk add --virtual build-deps gcc python-dev musl-dev && \
       apk add postgresql-dev && \
       apk add netcat-openbsd

   # set working directory
   WORKDIR /usr/src/app

   # set environment varibles
   ENV PYTHONDONTWRITEBYTECODE 1
   ENV PYTHONUNBUFFERED 1
   ENV FLASK_ENV production
   ENV APP_SETTINGS project.config.ProductionConfig

   # add and install requirements
   COPY ./requirements.txt /usr/src/app/requirements.txt
   RUN pip install -r requirements.txt

   # add app
   COPY . /usr/src/app

   # add and run as non-root user
   RUN adduser -D myuser
   USER myuser

   # run gunicorn
   CMD gunicorn --bind 0.0.0.0:$PORT manage:app

Una vez tenemos preparado el dockerfile pasamos a configurar todo para desplegar la apliccaion en HEROKU. Primero nos damos de alta e instalamos el `CLI <https://devcenter.heroku.com/articles/heroku-cli>`_. Para instalar en Ubuntu

.. code-block:: bash

   sudo snap install --classic heroku
   heroku version
   heroku login -i
   heroku: Enter your login credentials
   Email: jaagirre@mondragon.edu
   Password: **************
   Logged in as jaagirre@mondragon.edu

Una vez instalado el CLI de Heroku pasamos a crear la aplicacion,logearnos en el registro de contenedores de Heroku  y crearemos la base de datos en Heroku. Despues crearemos la imagen del contendeor de nuestra apliccaion y la tagearemos corrcetamente.  Despeus probaremos que la base dedatos esta alcanzable por el contenedor de forma local , y validaremos que podemos trabjar con la base de datos aproviiosnada en  Heroku. Una vez hemos realizado esto registaremos en Heroku nuestro contenedor y finalmente ejecutamos el contenedor. Por ultimo probaremos que todo esta correctamente, puede que existan probelmas con la base de datos,para ello tenemos los comandos CLI de nuestra aplicacion para crear y rellenar con datos iniciales creados en *manage.py*. A continuación resumiremos todos los comandos a realizar en cad uno de los pasos ;-)


#. 
   Crear la aplicacion Heroku (en este caso se llama *ancient-tundra-17646* ) 

   .. code-block:: bash

      $ heroku create
      Creating app... done, ⬢ ancient-tundra-17646
      https://ancient-tundra-17646.herokuapp.com/ | https://git.heroku.com/ancient-tundra-17646.git

#. 
   Logearnos en el registro de contenedores de Heroku

   .. code-block:: bash

      $ heroku container:login
      Login Succeeded

#. 
   Aprovisionar/crear la BBDD (en el plan gratuito hobby-dev)

   .. code-block:: bash

      heroku addons:create heroku-postgresql:hobby-denv
      Creating heroku-postgresql:hobby-dev on ⬢ ancient-tundra-17646... free
      Database has been created and is available
      ! This database is empty. If upgrading, you can transfer
      ! data from another database with pg:copy
      Created postgresql-shaped-54299 as DATABASE_URL
      Use heroku addons:docs heroku-postgresql to view documentation

#. 
   Crear la imagen del contenedor de producción (lo tageamos con la palabara web porque en  HEROKU existen  dos tipo de apps *workers* y *webs* y dos tipos de addons colas y bbdd [dynos]https://www.heroku.com/dynos)

   .. code-block:: bash

      docker build -f Dockerfile.prod -t registry.heroku.com/ancient-tundra-17646/web .

#. 
   Probar localmente el contenedor junto con la BBDD de Heroku
   Antes de probar con la nueva base de dadtos obtenemos la nueva URL y la ponemos como variable de entorno

.. code-block:: bash

   $ heroku config:get DATABASE_URL
   postgres://hukvgmbqgogica:dccad5ace36de2edef0af81179c5fa5fa42d1e325c11fbc674cfb8cc3395c4a0@ec2-107-21-200-103.compute-1.amazonaws.com:5432/d5r5m9qo7q0qlo
   export DATABASE_URL=postgres://hukvgmbqgogica:dccad5ace36de2edef0af81179c5fa5fa42d1e325c11fbc674cfb8cc3395c4a0@ec2-107-21-200-103.compute-1.amazonaws.com:5432/d5r5m9qo7q0qlo

Y ahora probamos localmente la imagen y la bbdd

.. code-block:: bash

   docker run --name flask-tdd -e "PORT=8765" -p 5002:8765 registry.heroku.com/ancient-tundra-17646/web:latest

probamos la url http://10.100.199.200:5002/ping y en caso de que funcione nos indica que todo va bien.  Si probamosla base dedatos con la url de /users , veremos que existen un error interno. Esto ocurre por que las tablas no se han creado (no se han eecjutado lo sscript SQL de postgress). Para poder crear la base ded datos utilizaremos el CLI  *recreat* que creamos, pero este no l opodremos ejecutar localmente por restricciones de Heroku. Esto lo haremos sobr eel contenedor desplegado en Heroku. Primero borramos la imagen localmente y vamos a desplegar el contendeor en Heroku.


#. 
   registar en Heroku la imagen productiva

   .. code-block:: bash

      docker push registry.heroku.com/ancient-tundra-17646/web:latest

#. 
   Ejecutar la imagen

   .. code-block:: bash

      heroku container:release web

#. 
   Probar la aplicacion y gestionar la BBDD
   Ahora navegamos a la url de Heroku y tenemos la aplicacion en marcha. Si probamos la URL */users* nos da un error interno debido a que las tablasno estan creadas, esto lo podemos comporbar viendo lo slogs de la aplicacion.

.. code-block:: bash

   heroku logs

Par asolventar esto ejecutaremos en el contenedor los CLI creado enmanage.py para crear las tablas y rellenarlas con datos iniciales.

.. code-block:: bash

   heroku run python manage.py recreate_db
   heroku run pyhton manage.py seed_db

Y si ahora navgemos a la URL */users* veremos los usuarios por defecto.

.. code-block:: json

   [
       {"id": 1, "username": "aitor", "email": "aitor@gmail.com", "created_date": null},
       {"id": 2, "username": "gorka", "email": "gorka@gmail.com", "created_date": null}]

Por ultimo probaremos el POST de un user mediante *httpie*.Si no est ainstalado instalarlo.

.. code-block:: bash

   $ http --json POST https://ancient-tundra-17646.herokuapp.com/users username=hello email=hello@world.com
   HTTP/1.1 201 CREATED
   Connection: keep-alive
   Content-Length: 42
   Content-Type: application/json
   Date: Fri, 24 Jan 2020 11:00:44 GMT
   Server: gunicorn/20.0.4
   Via: 1.1 vegur

   {
       "message": "hello@world.com was added!"
   }

Y si pedimos la lista de usuarios vemos el nuevo usuario ;-)

.. code-block:: json

   [{"id": 1, "username": "aitor", "email": "aitor@gmail.com", "created_date": null}, {"id": 2, "username": "gorka", "email": "gorka@gmail.com", "created_date": null}, {"id": 3, "username": "hello", "email": "hello@world.com", "created_date": null}]
