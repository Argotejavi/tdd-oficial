# TDD and Continuous integration  with docker, python flask, gitlab and heroku: Cobertura y calidad de código


Para realizar la cobertura de código utilizaremos el modulo *Coverage.py*. Este modulo se puede integrar a PyTest mediante pytest-cov. Lo primero que haremos será integrar la dependencia en *Requirements.txt*.

```txt
pytest-cov==2.8.1
```

El siguiente paso es configurar la cobertura mediante el fichero *.coveragerc*. Lo primero que se debe de realizar es quitar los ficheros de test del análisis/resultado de cobertura.  También podemos  indicarle que realice una cobertura de caminos/ramas de ejecución [Branch_coverage](https://coverage.readthedocs.io/en/latest/branch.html).


```config
[run]
omit = project/test/*
branch = True
```
Ahora volvemos a crear el contenedor de forma que podemos ejecutar la cobertura. Para que recoja los datos de cobertura es necesario tener un fichero __init__.py (aunque esté vacío) dentro de la carpeta de testeos y de las carpetas de código.

```bash
docker-compose up -d --build
docker-compose exec users python -m pytest 'project/test' -p no:warnings --cov='project'
---- coverage: platform linux, python 3.8.0-final-0 ------
Name                      Stmts   Miss Branch BrPart  Cover
-----------------------------------------------------------
project/__init__.py          19      1      0      0    95%
project/api/__init__.py       0      0      0      0   100%
project/api/models.py        13      0      0      0   100%
project/api/ping.py           8      0      0      0   100%
project/api/users.py         40      0      4      0   100%
project/config.py            12      0      0      0   100%
-----------------------------------------------------------
TOTAL                        92      1      4      0    99%

============================== 11 passed in 1.64s ==
```

El siguiente paso es asegurar un mínimo en la calidad del software con el objetivo de mejorar la mantenibilidad de este. Para ello en el mundo python es fundamental el modulo de Linting (verificación de código y errores de programación) Flake8. Por lo tanto lo primero que vamos a hacer es agregar la dependencia.

```bash
flake8==3.7.9
```

Para configurar flake8 se utiliza el fichero *setup.cfg*. Por ejemplo la siguiente configuración especifica el número máximo de caracteres por línea de código y vemos que tenemos una serie de errores de estilo de código.

```config
[flake8]
max-line-length = 119
```

Y ahora volvemos a crear la imagen, lo ponemos en marcha y ejecutamos flake8.
```bash
docker-compose up -d --build
docker-compose exec users flake8 project
project/test/functional/test_ping.py:11:1: W293 blank line contains whitespace
project/test/unit/test_config.py:5:1: E302 expected 2 blank lines, found 1
project/test/unit/test_config.py:11:1: E302 expected 2 blank lines, found 1
project/test/unit/test_config.py:18:1: E302 expected 2 blank lines, found 1
project/test/unit/test_config.py:22:88: W292 no newline at end of file
```

Para formatear el código automáticamente tenemos la aplicación **Black**, el cual aplica formato  automáticamente, convirtiéndolo transparente para el desarrollador. Primero instalamos la dependencia y después ejecutamos *Black* para que nos indique las propuestas de formato. para ver las diferencias ejecutamos Black con la opción diff y después si queremos aplicarlas aplicamos Black;-)

```config
black==19.10b0
```

```bash
docker-compose up -d --build
docker-compose exec users black project --check
would reformat /usr/src/app/project/api/ping.py
would reformat /usr/src/app/project/config.py
would reformat /usr/src/app/project/test/conftest.py
would reformat /usr/src/app/project/api/users.py
would reformat /usr/src/app/project/test/functional/test_ping.py
would reformat /usr/src/app/project/test/unit/test_config.py
would reformat /usr/src/app/project/test/test_users.py
Oh no! 💥 💔 💥
9 files would be reformatted, 2 files would be left unchanged.
```


```bash
docker-compose exec users black project --diff
docker-compose exec users black project
```

Ahora aplicamos el formato y probamos flake8 y los test para ver que todo continua igual.
```bash
docker-compose exec users python -m pytest 'project/test' -p no:warnings --cov='project'
docker-compose exec users flake8 project
project/api/models.py:4:1: F401 'sqlalchemy.inspection.inspect' imported but unused
project/api/users.py:1:1: F401 'sqlalchemy.exc' imported but unused
project/api/users.py:3:1: F401 'flask.jsonify' imported but unused
project/api/users.py:9:1: F401 'json' imported but unused
project/api/users.py:45:9: E722 do not use bare 'except'
project/test/test_users.py:4:1: F401 'project.db' imported but unused
```

En mi caso ahora me indica que algunos ficheros contienen import que luego no se utilizan. Otra herramienta para el formato es *isort* el cual ordena alfabéticamente todos los import.

```config
isort==4.3.21
```


Ahora volvemos a lanzar el  contenedor y validamos los posible cambios que quiere realizar *isort*. Y si queremos los aceptamos ;-)

```bash
docker-compose up -d --build
docker-compose exec users /bin/sh -c "isort project/*/*.py --check-only"
docker-compose exec users /bin/sh -c "isort project/*/*.py --diff"
docker-compose exec users /bin/sh -c "isort project/*/*.py"
```

Finalmente verificamos otra vez los testeos, el formato de código con flake8, black e isort. Y si todo se cumple correctamente hacemos el commit ;-)










