
Cobertura y calidad de código
=============================

Para realizar la covertura de código utilizaremos el modulo *Coverage.py*. Este modulo se puede intregar a PyTest mediante pytest-cov. Lo primero que haremos sera integrar la dependencia en *Requirements.txt*.

.. code-block:: txt

   pytest-cov==2.8.1

El siguiente paso es configurar la la covertura mediante el fichero *.coveragerc*. Lo primero que se debe de relaizra es quitar los ficheros de test del analisis/resultado de covertura.  Tambien podemos  indicarle que realiza una covertura de caminos/ramas de ejecución `Branch_coverage <https://coverage.readthedocs.io/en/latest/branch.html>`_.

.. code-block:: config

   [run]
   omit = project/test/*
   branch = True

Ahora volvemos a crear el contenedor de forma que podemos ejecutar la covertura.

.. code-block:: bash

   docker-compose up -d --build
   docker-compose exec users python -m pytest 'project/test' -p no:warnings --cov='project'
   ---- coverage: platform linux, python 3.8.0-final-0 ------
   Name                      Stmts   Miss Branch BrPart  Cover
   -----------------------------------------------------------
   project/__init__.py          19      1      0      0    95%
   project/api/__init__.py       0      0      0      0   100%
   project/api/models.py        13      0      0      0   100%
   project/api/ping.py           8      0      0      0   100%
   project/api/users.py         40      0      4      0   100%
   project/config.py            12      0      0      0   100%
   -----------------------------------------------------------
   TOTAL                        92      1      4      0    99%

   ============================== 11 passed in 1.64s ==

El siguiente paso es asegurar un minimo en la calidad del software con el objetivo d emejorar la mantenibilidad de este. Para ello en el mundo python es fundamental el modulo de Linting (verificacion de código y errores de programación) Flake8. Por lo tanto lo primero qu evamos a hacer es agregar la dependencia.

.. code-block:: bash

   flake==3.7.9

Para configurar flake8 se utiliza el fichero *setup.cfg*. Por ejemplo la siguienet configuracion especifica el numero maximo de caracteres por linea de código y vemos que tenemos un aserie de errores de estilo de codigo.

.. code-block:: config

   [flake8]
   max-line-length = 119

Y ahora volvemos a crear la imagen, lo ponemos en marcha y ejecutamso flake8.

.. code-block:: bash

   docker-compose up -d --build
   docker-compose exec users flake8 project
   project/test/functional/test_ping.py:11:1: W293 blank line contains whitespace
   project/test/unit/test_config.py:5:1: E302 expected 2 blank lines, found 1
   project/test/unit/test_config.py:11:1: E302 expected 2 blank lines, found 1
   project/test/unit/test_config.py:18:1: E302 expected 2 blank lines, found 1
   project/test/unit/test_config.py:22:88: W292 no newline at end of file

Par aformatear el codigo automaticamente tenemos la aplicacion **Black**\ , el cual aplica formato  automaticamente, convirtiendolo tarnsparanete para el desarrollador. Primero instalamos la dependencia y despues ejecutamos *Black* para qu enos indcique las propuestas de formato. para ver las diferencias ejecutamos Black con la opcion diff y despues si queremos aplicarlas aplicamos Black;-)

.. code-block:: config

   black==19.10b0

.. code-block:: bash

   docker-compose up -d --build
   docker-compose exec users black project --check
   would reformat /usr/src/app/project/api/ping.py
   would reformat /usr/src/app/project/config.py
   would reformat /usr/src/app/project/test/conftest.py
   would reformat /usr/src/app/project/api/users.py
   would reformat /usr/src/app/project/test/functional/test_ping.py
   would reformat /usr/src/app/project/test/unit/test_config.py
   would reformat /usr/src/app/project/test/test_users.py
   Oh no! 💥 💔 💥
   9 files would be reformatted, 2 files would be left unchanged.

.. code-block:: bash

   docker-compose exec users black project --diff
   docker-compose exec users black project

Ahora aplicanos el formato y prbamos flake8 y los test para ver que todo contiua igual.

.. code-block:: bash

   docker-compose exec users pytest 'proejct/test' --cov='project'
   docker-compose exec users flake8 project
   project/api/models.py:4:1: F401 'sqlalchemy.inspection.inspect' imported but unused
   project/api/users.py:1:1: F401 'sqlalchemy.exc' imported but unused
   project/api/users.py:3:1: F401 'flask.jsonify' imported but unused
   project/api/users.py:9:1: F401 'json' imported but unused
   project/api/users.py:45:9: E722 do not use bare 'except'
   project/test/test_users.py:4:1: F401 'project.db' imported but unused

En mi caso ahora me indica que algunos ficheros contienen import que luego no se utilizan.Otra herramienta para el formato es *isort* el cual ordena alfabeticamente todos los import.

.. code-block:: config

   isort==4.3.21

Ahora volvemos a lanzar el  contenedor y valiamods os posibel cambios que quiere realziar *isort*. Y si queremos los aceptamos ;-)

.. code-block:: bash

   docker-compose up -d --build
   docker-compose exec users /bin/sh -c "isort project/*/*.py --check-only"
   docker-compose exec users /bin/sh -c "isort project/*/*.py --diff"
   docker-compose exec users /bin/sh -c "isort project/*/*.py"

Finalmente verificamos otra vez los testeos , el formato de codigo con flake8,black e isort. Y si todo se cumple correctamente hacemos el commit ;-)
