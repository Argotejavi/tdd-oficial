# TDD and Continuous integration  with docker , python flask , gitlab and heroku : Integracion Continua

En este paso vamos  a automatizar el proceso de  validación del software y su posterior creación mediante el uso de un PIPELINE. Para automatizar el proceso se utiliza GITLAB . El objetivo del PIPELINE es:

1. Crear la imagen del Contenedor
2. Ejecutar las validaciones
3. En caso satisfactorio subir las imágenes al registro de Contenedores


En esta fase podemos trabajar tanto con el servidor gotlab.danz.eus como con gitlab.com. En lo siguientes pasos utilizaremos el servidor **gitlab.com**

Lo primeros que haremos será asegurar que el repositorio para el proyecto contiene el código creado hasta este momento. Una vez validado el siguiente paso es crear el fichero **.gitlab-ci.yml**. En este fichero se define el PIPELINE de CI. Se definen dos fases: 1) build y 2) test. En la fase de build se creara el contenedor y se registra la imagen. En la fase de validación se valida la funcionalidad junto con la calidad del código. La idea es que en caso de que la validación sea incorrecta no se registre el contenedor.

```yaml
image: docker:stable

stages:
  - build
  - test

variables:
  IMAGE: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}

build:
  stage: build
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $IMAGE:latest || true
    - docker build
        --cache-from $IMAGE:latest
        --tag $IMAGE:latest
        --file ./Dockerfile.prod
        "."
    - docker push $IMAGE:latest

test:
  stage: test
  image: $IMAGE:latest
  services:
    - postgres:latest
  variables:
    POSTGRES_DB: users
    POSTGRES_USER: runner
    POSTGRES_PASSWORD: ""
    POSTGRES_HOST_AUTH_METHOD: trust
    DATABASE_TEST_URL: postgres://runner@postgres:5432/users
  script:
    - pytest "project/test" -p no:warnings
    - flake8 project
    - black project --check
    - isort project/**/*.py --check-only

```

Para poder ejecutar los PIPELINES en gitlab existen dos posibilidades: 

1. Utilizar Runners propios
2. Utilizar Runners compartidos de GITLAB

En este caso utilizáremos la primera opción ya que la segunda opción solo esta disponible en *gitlab.com*, donde se ofrecen 2000min de computación por usuario y mes en la capa gratuita para la ejecución de los PIPELINES.

Una vez conectados a *gitlab.danz.eus*, y una vez seleccionado el proyecto, lo primero que hacemos es al apartado de settings del proyecto y activamos tanto la opción de pipelines como el de registro de contenedores. A partir de ahora dentro de vuestro proyecto debéis de tener las opciones de *packages/Container Registry* y *CI/CD*.

El siguiente paso es activar el servidor donde se ejecutara el PIPELINE, el cual se comunicara con el servidor de GITLAB. Para ello dentro de las opciones *Settings* seleccionamos la opción *CI/CD*. En esta sección podemos configurar variables de entorno , como por ejemplo secretos, URL o IPs y también los *RUNNERS* , es decir quien ejecutar los PIPELINES. Expandimos la sección *RUNNERS* y vamos a Specific runners. Y vamos a activar vuestro servidor VAGRANT como RUNNER. Para esto se debe de instalar el SW *Gitlab Runner* en vuestro servidor VAGRANT. Para ello seguir los siguientes pasos:

```bash
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo /usr/local/bin/gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
```


Y a continuación registramos el runner para un proyecto (y también podemos desregistrarlo). En el fichero /etc/gitlab-runner/config.toml  se guardan las configuraciones. En algunas situaciones se debe de configurar el modo privileged=true en el runner. para registrar el runner debéis de utilizar el token de registro que os indica la sección de *SETTINGS/CI_CD/RUNNERS/SpecificRunner*. El runner que instaláis es una imagen docker. Una vez ejecutado el registro esperamos un poco y veremos como en las opciones de *CI/CD* ya aparece como activo el Runner.

```bash
gitlab-runner register \
--non-interactive \
--executor "docker" \
--docker-image "docker:stable" \
--url "https://gitlab.danz.eus/" \
--registration-token "dkdkdlkKDs1wdghdgbdJCTut" \
--description "docker-runner" \
--tag-list "docker,aws" \
--run-untagged \
--locked="false"

 gitlab-runner unregister --url https://gitlab.danz.eus/ --token suMUJydjdghjdg6fW_VLvhdjushL

```
Una vez registrado, en caso de que lo hayamos registrado sin sudo requerirá ejecutar el runner manualmente para ello. Se recomienda ejecutar con sudo.
```bash
gitlab-runner run
```

Muchas veces el runner debe poder ejecutar el contenedor en modo privilegiado y por eso puede que tengáis que modificar la configuración del runner. Aquí tenemos una configuración ejemplo

```config
[[runners]]
  name = "docker-runner"
  url = "https://gitlab.danz.eus/"
  token = "uDiKqABam"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

De momento vamos a probar a ejecutar el pipeline. Primero lo probaremos manualmente, pero para ello el fichero *.gitlab-ci.yaml* debe de estar en el repositorio.

```bash
git add .
git commit -am 'ADDED .gitlab-ci.yaml '
git push origin master
```

Y vemos como automáticamente el pipeline comienza a ejecutarse. Para ello ir a la opción *CI/CD/pipelines* y veréis como debido al ultimo push se ha lanzado el pipeline con dos stages

Se podría criticar el anterior pipeline debido a que el push al registro se realiza antes de realizar el test, por ello nos parece más adecuado el siguiente pipeline. En el siguiente yaml se especifican tres fases , primero se construye la imagen, después se valida y finalmente se sube al registro. Se utilizan los tags para dar nombres correctos, y también se utiliza el cache para agilizar el build. se podrían especificar ciertos restricciones para limitar la ejecución de las diferentes fases (build, test y delivery), por ejemplo el delivery solo se hará en la rama master.

```yaml
image: gitlab/dind:latest

stages:
  - build
  - test
  - delivery

variables:
  IMAGE_LATEST: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:latest
  IMAGE_RELEASE: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:release_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}
  IMAGE_TEST: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:test_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}

before_script:


build:
  stage: build
  image: docker:stable
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $IMAGE_LATEST || true
    - docker build
        --cache-from $IMAGE_LATEST
        --tag $IMAGE_TEST
        --file ./Dockerfile.prod
        "."
    - docker push $IMAGE_TEST
   

   

test:
  stage: test
  image: $IMAGE_TEST
  services:
    - postgres:latest
  variables:
    POSTGRES_DB: users
    POSTGRES_USER: runner
    POSTGRES_PASSWORD: ""
    POSTGRES_HOST_AUTH_METHOD: trust
    DATABASE_TEST_URL: postgres://runner@postgres:5432/users
  script:
    - pytest "project/test" -p no:warnings
    - flake8 project
    - black project --check
    - isort project/**/*.py --check-only
   
delivery:
  stage: delivery
  image: docker:stable
  services:
    - docker:dind
  variables:
      DOCKER_DRIVER: overlay2
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $IMAGE_TEST
    - docker tag $IMAGE_TEST $IMAGE_RELEASE
    - docker push $IMAGE_RELEASE
    - docker tag $IMAGE_RELEASE $IMAGE_LATEST
    - docker push $IMAGE_LATEST
    
  only:
    refs:
      - master
      # - tags
    
```

Para terminar vamos a agregar un poco de branching y scrum a esta fase. la idea va a ser que se habrá una nueva petición por parte del usuario creando un Issue y se le asigne a un desarrollador. El desarrollador crea en local una rama y responde a la petición. Una vez validado el cambio sube su rama y realiza un merge request para master y así agregar sus cambios y crear una nueva imagen. 

Finalmente agregamos el historial de commits al readme.md , donde también se dibujara el estado del pipeline.

```markdown
[![pipeline status](https://gitlab.danz.eus/jaagirre/practica_ci_tdd/badges/master/pipeline.svg)](https://gitlab.danz.eus/jaagirre/practica_ci_tdd/commits/master)
```

