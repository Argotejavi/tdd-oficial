
Integracion Continua
====================

En este paso vamos  a automatizar el proceso de  validacion del software y su posterior creacion mediante el uso de un PIPELINE. Para automatizar el proceso se utiliza GITLAB . El objetivo del PIPELINE es:


#. Crear la imagen del Contenedor
#. Ejecutar las validaciones
#. En caso satisfactorio subir las imagenes al registro de Contenedores

En esta fase podmeos trabajar tanto con el servidor gotlab.danz.eus como con gitlab.com. En lo sisguientes pasos utilizaremos el servidor **gilab.com**

Lo primeros que haremos será asegurar que el repositorio para el proyecto contiene el código creado hasta este momento. Una vez validado el siguient paso es crear el fichero **.gitlab-ci.yml**. En este fichero se define el PIPELINE de CI. Se definen dos fases: 1) build y 2) test. En la fase de build se creara el contenedor y se registra la imagen. En lafase de validacion se valida la funcionalidad junto con la calidad del código. La idea es que en caso de que la validacion sea incorrecta no se registre el contenedor.

.. code-block:: yaml

   image: docker:stable

   stages:
     - build
     - test

   variables:
     IMAGE: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}

   build:
     stage: build
     services:
       - docker:dind
     variables:
       DOCKER_DRIVER: overlay2
     script:
       - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
       - docker pull $IMAGE:latest || true
       - docker build
           --cache-from $IMAGE:latest
           --tag $IMAGE:latest
           --file ./Dockerfile.prod
           "."
       - docker push $IMAGE:latest

   test:
     stage: test
     image: $IMAGE:latest
     services:
       - postgres:latest
     variables:
       POSTGRES_DB: users
       POSTGRES_USER: runner
       POSTGRES_PASSWORD: ""
       DATABASE_TEST_URL: postgres://runner@postgres:5432/users
     script:
       - pytest "project/test" -p no:warnings
       - flake8 project
       - black project --check
       - isort project/**/*.py --check-only

Para poder ejecutar los PIPELINES en gitlab existen dos posibilidades: 


#. Utilizar Runners propios
#. Utilizar Runners compartidos de GITLAB

En este caso utilziaremos la primera opcion ya que la segunda opcioón solo esta disponible en *gitlab.com*\ , donde se ofreecn 2000min de computacion por usuario y mes en la capa gratuita para la ejecucion de los PIPELINES.

Una vez conectados a *gitlab.danz.eus*\ , y una vez seleccionado el proyecto , lo primero que hacemos es al apartado de settings del proyecto y activamos tanto la opcion de pipelines como el de registro de contenedores. A partir de ahora dentro de vuestro proyecto debeis de tener las opciones de *packages/Container Registry* y *CI/CD*.

El siguiente paso es activar el servidor donde se ejecutara el PIPELINE, el cual se comunicara con el servdiro de GITLAB. Para ello dentro de las opciones *Settings* seleccionamos la opcion *CI/CD*. Ebn esta sección podemos configurar variables de entorno , como por ejemplo secretos, URL o IPs y tambien los *RUNNERS* , es decri quien ejecutar los PIPELINES. Expandimos la seccion *RUNNERS* y vamos a Specific runners. Y vamos a activar vuestro servidor VAGRANT como RUNNER. Para esto se debe de instalar el SW *Gitlab Runner* en vuestro servidor VAGRANT. Para ello seguir los siguientes pasos:

.. code-block:: bash

   sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
   sudo chmod +x /usr/local/bin/gitlab-runner
   sudo useradd --comment ‘GitLab Runner’ --create-home gitlab-runner --shell /bin/bash
   sudo /usr/local/bin/gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner

Y a continuacion registramos el runner para un proyecto (y tambien podemos desregistrarlo). En el fichero /etc/gitlab-runner/config.toml  se guardan las configuraciones.En algunas situaciones se debe de configurar el modo privileged=true en el runner. para regsitrar el runner debeis de utilizar el token de resgitro que os indica la seccion de *SERTTINGS/CI_CD/RUNNERS/SpecificRunner*. El runner que instalais es una imagen docker. Una vez ejecutado el registro esperamos un poco y veremos como en las opciones de *CI/CD* ya aparece como activo el Runner.

.. code-block:: bash

   gitlab-runner register \
   --non-interactive \
   --executor "docker" \
   --docker-image "docker:stable" \
   --url "https://gitlab.danz.eus/" \
   --registration-token "dkdkdlkKDs1wdghdgbdJCTut" \
   --description "docker-runner" \
   --tag-list "docker,aws" \
   --run-untagged \
   --locked="false"

    gitlab-runner unregister --url https://gitlab.danz.eus/ --token suMUJydjdghjdg6fW_VLvhdjushL

Una vez registrado, en caso de que lo hayamos registrado sin sudo requerira ejcuatr el runner manualmente para ello. SE recomienda ejecutar con sudo.

.. code-block:: bash

   gitlab-runner run

Muchas veces el runner debe poder ejecutar el contenedor en modo privilegiado y por eso puede que tengais que modificar la configuracion del runner. Aqui tenemos una configuracion ejempplo

.. code-block:: config

   [[runners]]
     name = "docker-runner"
     url = "https://gitlab.danz.eus/"
     token = "uDiKqABam"
     executor = "docker"
     [runners.custom_build_dir]
     [runners.docker]
       tls_verify = false
       image = "docker:stable"
       privileged = true
       disable_entrypoint_overwrite = false
       oom_kill_disable = false
       disable_cache = false
       volumes = ["/cache"]
       shm_size = 0
     [runners.cache]
       [runners.cache.s3]
       [runners.cache.gcs]

De momento vamos a probar a ejecura el pipeline. primero lo probaremos manualmente, pero para ello el fichero *.gitlab-ci.yaml* debe de estar en el repositorio.

.. code-block:: bash

   git add .
   git commit -am 'ADDED .gitlab-ci.yaml '
   git push origin master

Y vemos como automaticamente el pipeline comienza a ejecutarse. para ello ir a la opcion *CI/CD/pipelines* y vereis como debido al ultimo push se ha lanzado el pipeline con dos stages

Se podria criticar el anterior pipeline debido a que el push al registro se realiza antes de realizar el test, por ello nos parece mas adecuado el siguiente pipeline. En el siguiente yaml se especifican tres fases , primero se contruye la imagen , despues se valida y finalmente se sube al registro. Se utilizan los tags para dar nombres correctos, y tambien s eutilzia el cache para agilizar el build. se podrian especificar ciertos restriccione spara limitar la ejecución de las diferentes fases (build, test y delivery), por ejemplo el delivery solo se hara en l arama master.

.. code-block:: yaml

   image: gitlab/dind:latest

   stages:
     - build
     - test
     - delivery

   variables:
     IMAGE_LATEST: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:latest
     IMAGE_RELEASE: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:release_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}
     IMAGE_TEST: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:test_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}

   before_script:


   build:
     stage: build
     image: docker:stable
     services:
       - docker:dind
     variables:
       DOCKER_DRIVER: overlay2
     script:
       - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
       - docker pull $IMAGE_LATEST || true
       - docker build
           --cache-from $IMAGE_LATEST
           --tag $IMAGE_TEST
           --file ./Dockerfile.prod
           "."
       - docker push $IMAGE_TEST




   test:
     stage: test
     image: $IMAGE_TEST
     services:
       - postgres:latest
     variables:
       POSTGRES_DB: users
       POSTGRES_USER: runner
       POSTGRES_PASSWORD: ""
       DATABASE_TEST_URL: postgres://runner@postgres:5432/users
     script:
       - pytest "project/test" -p no:warnings
       - flake8 project
       - black project --check
       - isort project/**/*.py --check-only

   delivery:
     stage: delivery
     image: docker:stable
     services:
       - docker:dind
     variables:
         DOCKER_DRIVER: overlay2
     script:
       - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
       - docker pull $IMAGE_TEST
       - docker tag $IMAGE_TEST $IMAGE_RELEASE
       - docker push $IMAGE_RELEASE
       - docker tag $IMAGE_RELEASE $IMAGE_LATEST
       - docker push $IMAGE_LATEST

     only:
       refs:
         - master
         # - tags

Para terminar vamos a agregar un poco de branching y scrum a esta fase. la idea va a ser que se habra una nuev apeticion por parte del usuario creando un Issue y se le asigne a un desarrollador. El desarrollador crea en local una rama y responde a la peticion. Una vez validado el cambio subre su rama y realiza un merge request para master y asi agregar sus cambios y crear una nueva imagen. 

Finalmente agregamos el historial de commits al readme.md , odne tambien se dibujara el estado del pipeline.

.. code-block:: markdown

   [![pipeline status](https://gitlab.danz.eus/jaagirre/practica_ci_tdd/badges/master/pipeline.svg)](https://gitlab.danz.eus/jaagirre/practica_ci_tdd/commits/master)
