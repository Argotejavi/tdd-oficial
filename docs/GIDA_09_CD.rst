
Entrega y Despliegue Continua
=============================

En este paso el objetivo es agrgear el despliegue continuo en el pipeline. Como ejemplo se utilizara Heroku en el despliegue.

El primer paso es configurar la varibale *HEROKU_AUTH_TOKEN*\ , para utilizar a posteriori en el pipeline. para ello desde la consola ejecutamos el siguiente comando y con el valor obtenido creamos una variable de entorno encriptada en Gitlab.

.. code-block:: bash

   heroku auth:token
    ›   Warning: token will expire 01/23/2021
    ›   Use heroku authorizations:create to generate a long-term token
   a52djkjn-dmbvkldjd-dbcacf7703

A conitnuacion listamos uan serie de comando CLI de heroku que permiten obtener informacion de las aplicaciones actualmente desplegadas en Heroku.

.. code-block:: bash

   heroku apps
   heroku apps:info ancient-tundra-17646
   heroku open -a ancient-tundra-17646

Con el valor del token vamos a la configuracion CI/CD del proyecto y creamos la variable de entorno. Y ahora ya podemos agregar una fase de despligue al pippeline.

.. code-block:: yaml

   deploy:
     stage: deploy
     image: docker:stable
     services:
       - docker:dind
     variables:
         DOCKER_DRIVER: overlay2
         HEROKU_APP_NAME: ancient-tundra-17646
         HEROKU_REGISTRY_IMAGE: registry.heroku.com/${HEROKU_APP_NAME}
     script:
         - apk add --no-cache curl
         - chmod +x ./release.sh
         - docker build 
             --tag $HEROKU_REGISTRY_IMAGE
             --file ./Dockerfile.prod
             "."
         - docker login -u _ -p $HEROKU_AUTH_TOKEN registry.heroku.com
         - docker push $HEROKU_REGISTRY_IMAGE
         - ./release.sh 
     only:
       refs:
         - master

Y  agregamos el nuevo stage deploy

.. code-block:: yaml

   stages:
     - build
     - test
     - delivery
     - deploy

Y ahora agregamos el script *release.sh*\ , el cual mediante el uso del API de Heroku realizar un redespliegue. Al hacerlo mediante curl nos evitamos tener que instalar el CLI de heroku. Para redesplegar primero obtenems el ID de la imagen. Despues especificamos los datos que vamos a enviar en la peticion a realizar al API (updates,  type, docker-image ). Y finalmente mediante curl se llama al API par arealziar el update de la apliccaión.

.. code-block:: bash

   #!/bin/sh
   IMAGE_ID=$(docker inspect ${HEROKU_REGISTRY_IMAGE} --format={{.Id}})
   PAYLOAD='{"updates": [{"type": "web", "docker_image": "'"$IMAGE_ID"'"}]}'
   curl -n -X PATCH https://api.heroku.com/apps/$HEROKU_APP_NAME/formation \
     -d "${PAYLOAD}" \
     -H "Content-Type: application/json" \
     -H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
     -H "Authorization: Bearer ${HEROKU_AUTH_TOKEN}"
