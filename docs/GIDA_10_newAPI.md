# TDD and Continuous integration  with docker, python flask , gitlab and heroku: Implementacion nuevas API /delete y /put/:id

En esta nueva fase se va a finalizar utilizando TDD el API USERS. Para ello primero escribiremos el test, después la implementación y se refactorizará si es necesario. En el primer test partiendo de una base de datos vacía, primero insertamos un usuario y aseguramos que el usuario se ha insertado correctamente en la base de datos. A  continuación se realiza la petición de eliminación de usuario y se verifica que la respuesta HTTP es correcta y que el mensaje de respuesta indica que se ha eliminado el usuario. Finalmente se realiza la petición para obtener la lista de usuarios y está tiene que estar vacía.


## DELETE user

```python
# project/test/test_users.py

def test_remove_user(test_app, test_database , add_user):
    test_database.session.query(User).delete()
    user = add_user("user-to-be-removed" , "joseba@gmail.com")
    client = test_app.test_client()
    resp_one = client.get("/users")
    data = json.loads(resp_one.data.decode())
    assert resp_one.status_code == 200
    assert len(data) == 1
    resp_two = client.delete(f"/users/{user.id}")
    data = json.loads(resp_two.data.decode())
    assert resp_two.status_code == 200
    assert 'joseba@gmail.com was removed!' in data['message']
    resp_three = client.get('/users')
    data = json.loads(resp_three.data.decode())
    assert resp_three.status_code == 200
    assert len(data) == 0
```

Una vez escrito el test , pasamos a ejecutarlo en local. Para ello levantamos el docker-compose y ejecutamos los test, y veremos como el nuevo test no pasa correctamente. Concretamente nos indica que el método /DELETE no está accesible.

```bash
docker-compose up -d --build
docker-compose exec users python -m pytest 'project/test'
========= FAILURES ======
______ test_remove_user __
E       assert 405 == 200
E        +  where 405 = <Response 70 bytes [405 METHOD NOT ALLOWED]>.status_code
project/test/test_users.py:98: AssertionError
================ 1 failed, 11 passed,
```

Por lo tanto pasamos a implementarlo. Por lo tanto vamos a la clase Users e implementaremos el recurso/ruta delete.

```python
# project/api/users.py
#class Users(Resource):
 def delete(self, user_id):
    response_object={}
    user = User.query.filter_by(id=user_id).first()
    if not user:
        api.abort(404, f"User {user_id} does not exist")
    db.session.delete(user)
    db.session.commit()
    response_object["message"]=f"{user.email} was removed!"
    return response_object, 200
```

Y ahora al volver a ejecutar tendremos todo ok!!
```bash
===== 12 passed
```

En el siguiente test verificamos que si se pide modificar un id incorrecto la aplicación no falla.

```python 
def test_remove_user_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.delete("/users/9999")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User 9999 does not exist" in data['message']
```

Y como en el código anterior ya hemos contemplado con el if esta situación todo esta correcto, teniendo 13 test correctos.

```bash
======= 13 passed
```

Ahora vamos a por la url de modificación.

# PUT /users/:id

Primero vamos con el requisito. El test será el siguiente:
    - Insertamos en la BBDD directamente un usuario
        - Llamamos a la función del API PUT y le pasamos como datos un json con los nuevos valores
        - Vemos que la respuesta es OK
        - Y por último hacemos un GET para ver si los nuevo valores están accesibles correctamente

```python
def test_update_user(test_app, test_database, add_user):
    user = add_user("user-to-be-updated", "joseba@gmail.com")
    client = test_app.test_client()
    resp_one = client.put(
        f"/users/{user.id}",
        data=json.dumps({"username": "me", "email": "me@gmail.com"}),
        content_type="application/json",
    )
    data = json.loads(resp_one.data.decode())
    assert resp_one.status_code == 200
    assert f"{user.id} was updated!" in data["message"]
    resp_two = client.get(f"/users/{user.id}")
    data = json.loads(resp_two.data.decode())
    assert resp_two.status_code == 200
    assert "me" in data["username"]
    assert "me@gmail.com" in data["email"]
```

Si ahora ejecutamos el test obtendremos un error

```bash
==========FAILURE=================================
_______________ test_update_user _______
>       assert resp_one.status_code == 200
E       assert 405 == 200
E        +  where 405 = <Response 70 bytes [405 METHOD NOT ALLOWED]>.status_code

project/test/test_users.py:123: Ass
======= 1 failed, 13 passed,
```


Y ahora pasamos a implementar este requisito. Para ello creamos la función put al que le agregamos el decorador expect.

```bash
@api.expect(user, validate=True)
def put(self, user_id):
    post_data = request.get_json()
    username = post_data.get("username")
    email = post_data.get("email")
    response_object = {}

    user = User.query.filter_by(id=user_id).first()
    if not user:
        api.abort(404, f"User {user_id} does not exist")
    user.username = username
    user.email = email
    db.session.commit()
    response_object["message"] = f"{user.id} was updated!"
    return response_object, 200
```

A continuación agregamos los requisitos de que no se puede actualizar un usuario utilizando un json incorrecto. 


```python
def test_update_user_invalid_json(test_app, test_database):
    client = test_app.test_client()
    resp = client.put(
        "/users/1",
        data=json.dumps({}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Input payload validation failed" in data["message"]


    def test_update_user_invalid_json_keys(test_app, test_database):
    client = test_app.test_client()
    resp = client.put(
        "/users/1",
        data=json.dumps({"email": "me@testdriven.io"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Input payload validation failed" in data["message"]
```

Tampoco se podrá actualizar un usuario que no existe

```python



def test_update_user_does_not_exist(test_app, test_database):
    client = test_app.test_client()
    resp = client.put(
        "/users/999",
        data=json.dumps({"username": "me", "email": "me@testdriven.io"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User 999 does not exist" in data["message"]
```
## PARAMETRIZACION DE TESTS

Hasta ahora estamos ejecutando los test siempre con lo mismos datos y solamente con una variable datos.
La parametrización de test nos permite ejecutar un mismo test con diferentes datos de entrada;-)
Para trabajar este concepto utilizaremos los test de respecto al API PUT. En los tres test respecto a los PUT relacionados con datos erróneos, el código es prácticamente el mismo, solo cambiando los datos de entrada. Con el objetivo de simplificar el código entran en juego los parámetros de los tests. Con el siguiente código ejecutaremos un mismo test varias veces.

```python
import pytest

@pytest.mark.parametrize("user_id, payload, status_code, message", [
    [1, {}, 400, "Input payload validation failed"],
    [1, {"email": "me@gmail.com"}, 400, "Input payload validation failed"],
    [999, {"username": "me", "email": "me@gmail.com"}, 404, "User 999 does not exist"],
])
def test_update_user_invalid_with_params(test_app, test_database, user_id, payload, status_code, message):
    client = test_app.test_client()
    resp = client.put(
        f"/users/{user_id}",
        data=json.dumps(payload),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == status_code
    assert message in data["message"]
```

Y si ahora ejecutamos el test tendremos 20 tests ;-)
```bash
==== 20 passed,
```

Para terminar con esta fase analizamos como esta nuestra cobertura

```bash
 docker-compose exec users pytest 'project/test' -p no:warnings --cov='project'
 ----------- coverage: platform linux, python 3.8.0-final-0 -----------
Name                      Stmts   Miss Branch BrPart  Cover
-----------------------------------------------------------
project/__init__.py          19      1      0      0    95%
project/api/__init__.py       0      0      0      0   100%
project/api/models.py        12      0      0      0   100%
project/api/ping.py           8      0      0      0   100%
project/api/users.py         61      0      8      0   100%
project/config.py            12      0      0      0   100%
-----------------------------------------------------------
TOTAL                       112      1      8      0    99%

=============================== 20 passed in 2.01s =====
```

Finalmente volvemos a chequear el formato del código con flake8 , lo formateamos si hace falta con Black y si todo esta bien lo subimos al repositorio y hacemos el MERGE_REQUEST ;-)

```bash
docker-compose exec users flake8 project
$ docker-compose exec users black project
$ docker-compose exec users /bin/sh -c "isort project/*/*.py"
```

Y probamos en HEROKU!!!!  

