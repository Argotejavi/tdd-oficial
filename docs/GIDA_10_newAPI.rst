
Implementacion nuevas API /delete y /put/:id
============================================

En esta nueva fase se va a finalizar utilizando TDD el API USERS. Para ello  primero escribiremos el test , despues la implementacion y se refactorizada si es enecsario.En elprimer test partiendo de un abase de datos vacia , primero insertamos un usuario y aseguramos que el usuario se ha insertado corrcetamente en la base de datos. A  continuacion se realiza la peticion de eliminacion de usuario y se verifica que la respuesta HTTP es correcta y qu eel mensaje de respuesta indica que se ha eliminado el usuario. Finalmente se realiza la peticion par aobtener la lista de usuarios y esta tiene que estar vacia.

DELETE user
-----------

.. code-block:: python

   # project/test/test_users.py

   def test_remove_user(test_app, test_database , add_user):
       test_database.session.query(User).delete()
       user = add_user("user-to-be-removed" , "joseba@gmail.com")
       client = test_app.test_client()
       resp_one = client.get("/users")
       data = json.loads(resp_one.data.decode())
       assert resp_one.status_code == 200
       assert len(data) == 1
       resp_two = client.delete(f"/users/{user.id}")
       data = json.loads(resp_two.data.decode())
       assert resp_two.status_code == 200
       assert 'joseba@gmail.com was removed!' in data['message']
       resp_three = client.get('/users')
       data = json.loads(resp_three.data.decode())
       assert resp_three.status_code == 200
       assert len(data) == 0

Una vez escrito el test , pasamos a ejecutarlo en local. Para ello levantamos el docker-compose y ejecutamos los test , y veremos como el nuevo test no pasa correctamente. Concretamente nos indica que el metodo /DELETE no esta accesible.

.. code-block:: bash

   docker-compose up -d --build
   docker-compose exec users python 'project/test'
   ========= FAILURES ======
   ______ test_remove_user __
   E       assert 405 == 200
   E        +  where 405 = <Response 70 bytes [405 METHOD NOT ALLOWED]>.status_code
   project/test/test_users.py:98: AssertionError
   ================ 1 failed, 11 passed,

Por lo tanto pasamos a implementarlo. Por lo tanto vamos qala clase Users e implementarems el recurso/ruta delete.

.. code-block:: python

   # project/api/users.py
   #class Users(Resource):
    def delete(self, user_id):
       response_object={}
       user = User.query.filter_by(id=user_id).first()
       if not user:
           api.abort(404, f"User {user_id} does not exist")
       db.session.delete(user)
       db.session.commit()
       response_object["message"]=f"{user.email} was removed!"
       return response_object, 200

Y ahora al volver a ejecutar tendremos todo ok!!

.. code-block:: bash

   ===== 12 passed

En el siguiente test verificamos que si se pide modificar un id incorrecto la aplicacion no falla.

.. code-block:: python

   def test_remove_user_incorrect_id(test_app, test_database):
       client = test_app.test_client()
       resp = client.delete("/users/9999")
       data = json.loads(resp.data.decode())
       assert resp.status_code == 404
       assert "User 9999 does not exist" in data[message]

Y como en el codigo anterior ya hemeos contemplado con el if esta situacio todo esta correcto , teniendo 13 test correctos.

.. code-block:: bash

   ======= 13 passed

Ahora vamos a por la url de modificaión.

PUT /users/:id
==============

Primero  vamos con el requisito. El test sera el siguiente:

.. code-block::

   - Insertamos en la BBDD directamente un usuario
   - Llamamos a la funcion del API PUT y le pasamos como datos un json con los nuevos valores
   - Vemos que la respuesta es OK
   - Y por ultimo hacemos un GET para ver si los nuevo svalores estan accesibles correctamente


.. code-block:: python

   def test_update_user(test_app, test_database, add_user):
       user = add_user("user-to-be-updated", "joseba@gmail.com")
       client = test_app.test_client()
       resp_one = client.put(
           f"/users/{user.id}",
           data=json.dumps({"username": "me", "email": "me@gmail.com"}),
           content_type="application/json",
       )
       data = json.loads(resp_one.data.decode())
       assert resp_one.status_code == 200
       assert f"{user.id} was updated!" in data["message"]
       resp_two = client.get(f"/users/{user.id}")
       data = json.loads(resp_two.data.decode())
       assert resp_two.status_code == 200
       assert "me" in data["username"]
       assert "me@gmail.com" in data["email"]

Si ahroa ejecutamos el test obtendremos un error

.. code-block:: bash

   ==========FAILURE=================================
   _______________ test_update_user _______
   >       assert resp_one.status_code == 200
   E       assert 405 == 200
   E        +  where 405 = <Response 70 bytes [405 METHOD NOT ALLOWED]>.status_code

   project/test/test_users.py:123: Ass
   ======= 1 failed, 13 passed,

Y ahora pasamos a implementar este requisito. Para ello creamos la funcion put al que le agrgamos el decorador expect.

.. code-block:: bash

   @api.expect(user, validate=True)
   def put(self, user_id):
       post_data = request.get_json()
       username = post_data.get("username")
       email = post_data.get("email")
       response_object = {}

       user = User.query.filter_by(id=user_id).first()
       if not user:
           api.abort(404, f"User {user_id} does not exist")
       user.username = username
       user.email = email
       db.session.commit()
       response_object["message"] = f"{user.id} was updated!"
       return response_object, 200

A continuación agregamos los requisitos de que no se puede actualizar un usuario utilizando un json incorrecto. 

.. code-block:: python

   def test_update_user_invalid_json(test_app, test_database):
       client = test_app.test_client()
       resp = client.put(
           "/users/1",
           data=json.dumps({}),
           content_type="application/json",
       )
       data = json.loads(resp.data.decode())
       assert resp.status_code == 400
       assert "Input payload validation failed" in data["message"]


       def test_update_user_invalid_json_keys(test_app, test_database):
       client = test_app.test_client()
       resp = client.put(
           "/users/1",
           data=json.dumps({"email": "me@testdriven.io"}),
           content_type="application/json",
       )
       data = json.loads(resp.data.decode())
       assert resp.status_code == 400
       assert "Input payload validation failed" in data["message"]

Tampoco se podra actualizar un usuario que no existe

.. code-block:: python




   def test_update_user_does_not_exist(test_app, test_database):
       client = test_app.test_client()
       resp = client.put(
           "/users/999",
           data=json.dumps({"username": "me", "email": "me@testdriven.io"}),
           content_type="application/json",
       )
       data = json.loads(resp.data.decode())
       assert resp.status_code == 404
       assert "User 999 does not exist" in data["message"]

PARAMETRIZACION DE TESTS
------------------------

Hasta ahora estamos ejecutando los test siempore con lo smismo datos y solamente con una varibale datos.
La parametrizacion de test nos permite ejecutar un mismo test con diferentes datos de entrada;-)
Para trabajar este conepto utilizaremos los test de respecto al API PUT. En los tres  test respecto a los PUT relacionados ocn datos erroneos , el codigo es practicamente el mismo , solo cambiando los datsp de entrada. Con el objetivo de simplificar el codigo entran en juego los paranetros de los tests. Con el siguiente código ejecutaremso un mismo test varias veces.

.. code-block:: python

   import pytest

   @pytest.mark.parametrize("user_id, payload, status_code, message", [
       [1, {}, 400, "Input payload validation failed"],
       [1, {"email": "me@gmail.com"}, 400, "Input payload validation failed"],
       [999, {"username": "me", "email": "me@gmail.com"}, 404, "User 999 does not exist"],
   ])
   def test_update_user_invalid_with_params(test_app, test_database, user_id, payload, status_code, message):
       client = test_app.test_client()
       resp = client.put(
           f"/users/{user_id}",
           data=json.dumps(payload),
           content_type="application/json",
       )
       data = json.loads(resp.data.decode())
       assert resp.status_code == status_code
       assert message in data["message"]

Y si ahora ejecutamos el test tendremos 20 tests ;-)

.. code-block:: bash

   ==== 20 passed,

Para terminar con esta fase anazliamos como esta nuestra covertura

.. code-block:: bash

    docker-compose exec users pytest 'project/test' -p no:warnings --cov='project'
    ----------- coverage: platform linux, python 3.8.0-final-0 -----------
   Name                      Stmts   Miss Branch BrPart  Cover
   -----------------------------------------------------------
   project/__init__.py          19      1      0      0    95%
   project/api/__init__.py       0      0      0      0   100%
   project/api/models.py        12      0      0      0   100%
   project/api/ping.py           8      0      0      0   100%
   project/api/users.py         61      0      8      0   100%
   project/config.py            12      0      0      0   100%
   -----------------------------------------------------------
   TOTAL                       112      1      8      0    99%

   =============================== 20 passed in 2.01s =====

Finalmente volvemos a chrequear el formato del código con flake8 , lo formateamos si hace falta con Black y si todo esta bien lo subimos al repositiorio y hacemso el MERGE_REQUEST ;-)

.. code-block:: bash

   docker-compose exec users flake8 project
   $ docker-compose exec users black project
   $ docker-compose exec users /bin/sh -c "isort project/*/*.py"

Y probamos en HEROKU!!!!  
