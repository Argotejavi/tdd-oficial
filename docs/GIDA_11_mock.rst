
Monkeypatching, Doubles y mocks
===============================

Mediante le uso de mocks modificaremos el funcionamiento de funciones externas a un componenete SW de forma que si estos compoenetes externos no estan implementados , o son de larga duracuión o tienes probelmas no afectan al desarrollo de nuestro componente software.  En nuestro caso utilziaremos la libreria *monkeypatch* para implementar los mocks.  mediante esta librerai podemos establecer el comportamiento concreto de una funcion , en nuestro caso de las llmadas a la base de datos;-)

Antes de comenzar a utilizar los mocks ,vamos a refactorizar un poco el codigo de forma que creamos una capa superio al modelo ORM ALCHEMY de usuario creando una capa de servicio o facade. Para esto dentro d ela carpeta API  crearemos el modulo *servces.py*. Mediante esta capa ofreceremos un acceso a la base de datos. antes de comenzar con estp aseguraros de que tenemos la ultima version d ela rama MASTER. Y comporbar que estamos en el commit correspodniente

.. code-block:: bash

   git pull origin master
   git log --graph
   *   commit 02b11b2e5a5fa86716a46bf836d69ff9fa4d8981
   | Merge: b5edc8e 381fb35
   | | Author: joseba a. agirre <jaagirre@mondragon.edu>
   | | Date:   Wed Feb 5 16:22:59 2020 +0000
   | |
   | |     Merge branch 'GIDA_10' into 'master'
   | |
   | |     Gida 10
   | |
   | |     See merge request jaagirre/flask-tdd-docker!3
   | |
   | * commit 381fb35523c0bc3ca6b0b7f00dd431271e1bd112
   | | Author: jaagirre <jaagirre@mondragon.edu>
   | | Date:   Wed Feb 5 16:18:55 2020 +0000
   | |
   | |     PARAMETRIZACION DE TEST OK
   | |
   | * commit b4d938b752dbe7845ca84c020d02df8a9cebccf6GIT 
   | | Author: jaagirre <jaagirre@mondragon.edu>
   | | Date:   Wed Feb 5 16:05:27 2020 +0000
   | |
   | |     API PUT IMPLEMENTADO
   | |
   | * commit 6b0cae24989e8534edef3106a5d482b0cf7b0273
   | | Author: jaagirre <jaagirre@mondragon.edu>
   | | Date:   Wed Feb 5 15:46:24 2020 +0000
   | |
   | |     CODIGO FORMATEADO MEDIANTE BLACK

Y ahora comenzamos a crear la capa de servicios en una nueva rama local , por ejemplo GIDA_11, o el nombre qu etenga el issue asignado a nosotros.

.. code-block:: python

   from project import db
   from project.api.models import User


   def get_all_users():
       return User.query.all()


   def get_user_by_id(user_id):
       return User.query.filter_by(id=user_id).first()


   def get_user_by_email(email):
       return User.query.filter_by(email=email).first()


   def add_user(username, email):
       user = User(username=username, email=email)
       db.session.add(user)
       db.session.commit()
       return user


   def update_user(user, username, email):
       user.username = username
       user.email = email
       db.session.commit()
       return user


   def delete_user(user):
       db.session.delete(user)
       db.session.commit()
       return user

El siguiente paso es modificar el modulo Users.py para que utilice la capa intermedia de base de datos. Lo primero es importar las funciones expuesta por el servicio.

.. code-block:: python

   # project/api/users.py
   # new
   from project.api.services import (
       get_all_users,
       get_user_by_email,
       add_user,
       get_user_by_id,
       update_user,
       delete_user
   )

Una vexz importadas las funciones el siguiente paso es modificar las llamadas al modelo ORM Users y sustituirlo por los servcios. Y comporbar que tras esta refactorizacion todo sigue igual , probando que los test siguen pasandose correctamente. Por ejemplo en la clase UsersList en el get  sustituimos

.. code-block:: python

   return get_all_users(), 200 #User.query.all(), 200

.. code-block:: python

   class UsersList(Resource):
       # @api.expect(user, validate=True) # new
       def post(self):
           post_data = request.get_json()
           username = post_data.get("username")
           email = post_data.get("email")
           response_object = {}

           user = get_user_by_email(email) # User.query.filter_by(email=email).first()
           if user:
               response_object["message"] = "Sorry. That email alreadey exists"
               return response_object, 400

           # Con el validador si lo sdatso no cumplen con el modelo no llega aqui
           try:
               # db.session.add(User(username=username, email=email))
               # db.session.commit()
               add_user(username, email) 
               response_object["message"] = f"{email} was added!"
               return response_object, 201
           except Exception:
               db.session.rollback()
               response_object["message"] = f"Input payload validation failed"
               return response_object, 400

       @api.marshal_with(user, as_list=True)
       def get(self):
           return get_all_users(), 200 #User.query.all(), 200


   class Users(Resource):
       @api.marshal_with(
           user
       )  # este decorador se utiliza para pasar los datso del get a un JSON
       def get(self, user_id):
           user = get_user_by_id(user_id) #  User.query.filter_by(id=user_id).first()
           if not user:
               api.abort(404, f"User {user_id} does not exist")
           return user, 200

       def delete(self, user_id):
           response_object = {}
           user =  get_user_by_id(user_id) # User.query.filter_by(id=user_id).first()
           if not user:
               api.abort(404, f"User {user_id} does not exist")
           # db.session.delete(user)
           # db.session.commit()
           delete_user(user)
           response_object["message"] = f"{user.email} was removed!"
           return response_object, 200

       @api.expect(user, validate=True)
       def put(self, user_id):
           post_data = request.get_json()
           username = post_data.get("username")
           email = post_data.get("email")
           response_object = {}

           user =  get_user_by_id(user_id) # User.query.filter_by(id=user_id).first()
           if not user:
               api.abort(404, f"User {user_id} does not exist")
           # user.username = username
           # user.email = email
           # db.session.commit()
           update_user(user, username, email)
           response_object["message"] = f"{user.id} was updated!"
           return response_object, 200

Una vez modificado el codigo los test deberan seguir pasandose correctamente , pero siguen interactuando con la base de datos y ahora queremos realizar test unitarios o de componentes.El siguinte paso es escribir test unitarios solo para el modulo Users sin que nos afecte la base de datos , para ello utilziaremos la capa de servicio y mockearemos estacapa mediante *monkeypatch*. Primero escribriemos los test el componente users sin codigo.

.. code-block:: python

   # project/test/test_users_unit.py
   import json
   from datetime import datetime

   import pytest

   import project.api.users


   def test_add_user(test_app, monkeypatch):
       pass


   def test_add_user_invalid_json(test_app, monkeypatch):
       pass


   def test_add_user_invalid_json_keys(test_app, monkeypatch):
       pass


   def test_add_user_duplicate_email(test_app, monkeypatch):
       pass


   def test_single_user(test_app, monkeypatch):
       pass


   def test_single_user_incorrect_id(test_app, monkeypatch):
       pass


   def test_all_users(test_app, monkeypatch):
       pass


   def test_remove_user(test_app, monkeypatch):
       pass


   def test_remove_user_incorrect_id(test_app, monkeypatch):
       pass


   def test_update_user(test_app, monkeypatch):
       pass


   @pytest.mark.parametrize(
       "user_id, payload, status_code, message",
       [
           [1, {}, 400, "Input payload validation failed"],
           [1, {"email": "me@gmail.com"}, 400, "Input payload validation failed"],
           [
               999,
               {"username": "me", "email": "me@gmail.com"},
               404,
               "User 999 does not exist",
           ],
       ],
   )
   def test_update_user_invalid(test_app, monkeypatch, user_id, payload, status_code, message):
       pass

Si levantamos los contenedores y eejcutamos los test tendremos

.. code-block:: bash

   ===== 33 passed,

Ahora comenzamos a escribir todos los test con sus mocks. Y ejecutamos;-) Primero comenzamos creando el test unitario de /POST de User.

.. code-block:: python


   def test_add_user(test_app, monkeypatch):
       def mock_get_user_by_email(email):
           return None
       def mock_add_user(username, email):
           return True
       monkeypatch.setattr(project.api.users, "get_user_by_email", mock_get_user_by_email)
       monkeypatch.setattr(project.api.users, "add_user", mock_add_user)

       client = test_app.test_client()
       resp = client.post(
           "/users",
           data=json.dumps({"username": "michael", "email": "urtzi@gmail.com"}),
           content_type="application/json",
       )
       data = json.loads(resp.data.decode())
       assert resp.status_code == 201
       assert "urtzi@gmail.com was added!" in data["message"]



   def test_add_user_duplicate_email(test_app, monkeypatch):
       def mock_get_user_by_email(email):
           return True
       def mock_add_user(username, email):
           return True
       monkeypatch.setattr(project.api.users, "get_user_by_email", mock_get_user_by_email)
       monkeypatch.setattr(project.api.users, "add_user", mock_add_user)
       client = test_app.test_client()
       client.post(
           "/users",
           data=json.dumps({"username": "michael", "email": "urtzi@gmail.com"}),
           content_type="application/json",
       )
       resp = client.post(
           "/users",
           data=json.dumps({"username": "michael", "email": "urtzi@gmail.com"}),
           content_type="application/json",
       )
       data = json.loads(resp.data.decode())
       assert resp.status_code == 400
       assert "Sorry. That  email already exists" in data["message"]

Ahora agregamps otros dos test para la creacion de usuarios, concretamente contemplamos los casos de datos erroneos. En este caso se utiliza el decorado de Flask , lo cual implica que no hay interaccion con la base de datso en caso de error , por ello mockeamos ninguna función.

.. code-block:: python

   def test_add_user_invalid_json(test_app):
       client = test_app.test_client()
       resp = client.post("/users", data=json.dumps({}), content_type="application/json",)
       data = json.loads(resp.data.decode())
       assert resp.status_code == 400
       assert "Input payload validation failed" in data["message"]


   def test_add_user_invalid_json_keys(test_app, monkeypatch):
       client = test_app.test_client()
       resp = client.post(
           "/users",
           data=json.dumps({"email": "john@gmail.com.io"}),
           content_type="application/json",
       )
       data = json.loads(resp.data.decode())
       assert resp.status_code == 400
       assert "Input payload validation failed" in data["message"]

A continuacion mockeamos los test referentes al API GET /user.

.. code-block:: python

   from datetime import datetime

   def test_single_user(test_app, monkeypatch):
       def mock_get_user_by_id(user_id):
           return {
               "id": 1,
               "username": "joseba",
               "email": "joseba@gmail.com",
               "created_date": datetime.now()
           }
       monkeypatch.setattr(project.api.users, "get_user_by_id", mock_get_user_by_id)
       client = test_app.test_client()
       resp = client.get("/users/1")
       data = json.loads(resp.data.decode())
       assert resp.status_code == 200
       assert "joseba" in data["username"]
       assert "joseba@gmail.com" in data["email"]


   def test_single_user_incorrect_id(test_app, monkeypatch):
       def mock_get_user_by_id(user_id):
           return None
       monkeypatch.setattr(project.api.users, "get_user_by_id", mock_get_user_by_id)
       client = test_app.test_client()
       resp = client.get("/users/999")
       data = json.loads(resp.data.decode())
       assert resp.status_code == 404
       assert "User 999 does not exist" in data["message"]


   def test_all_users(test_app, monkeypatch):
       def mock_get_all_users():
           return [
               {
                   "id": 1,
                   "username": "urtzi",
                   "email": "urtzi@gmail.com",
                   "created_date": datetime.now()
               },
               {
                   "id": 1,
                   "username": "irati",
                   "email": "irati@gmail.com",
                   "created_date": datetime.now()
               }
           ]
       monkeypatch.setattr(project.api.users, "get_all_users", mock_get_all_users)
       client = test_app.test_client()
       resp = client.get("/users")
       data = json.loads(resp.data.decode())
       assert resp.status_code == 200
       assert len(data) == 2
       assert "urtzi" in data[0]["username"]
       assert "urtzi@gmail.com" in data[0]["email"]
       assert "irati" in data[1]["username"]
       assert "irati@gmail.com" in data[1]["email"]

Ahora mockeamos las operaciones de /DELETE  .

.. code-block:: python

   def test_remove_user(test_app, monkeypatch):
       class AttrDict(dict):
           def __init__(self, *args, **kwargs):
               super(AttrDict, self).__init__(*args, **kwargs)
               self.__dict__ = self
       def mock_get_user_by_id(user_id):
           d = AttrDict()
           d.update({
               "id": 1,
               "username": "user-to-be-removed",
               "email": "joseba@gmail.com"
           })
           return d
       def mock_delete_user(user):
           return True
       monkeypatch.setattr(project.api.users, "get_user_by_id", mock_get_user_by_id)
       monkeypatch.setattr(project.api.users, "delete_user", mock_delete_user)
       client = test_app.test_client()
       resp_two = client.delete("/users/1")
       data = json.loads(resp_two.data.decode())
       assert resp_two.status_code == 200
       assert "joseba@gmail.com was removed!" in data["message"]


   def test_remove_user_incorrect_id(test_app, monkeypatch):
       def mock_get_user_by_id(user_id):
           return None
       monkeypatch.setattr(project.api.users, "get_user_by_id", mock_get_user_by_id)
       client = test_app.test_client()
       resp = client.delete("/users/999")
       data = json.loads(resp.data.decode())
       assert resp.status_code == 404
       assert "User 999 does not exist" in data["message"]

Y por ultimo mockeamos el UPDATE /users.

.. code-block:: python

   def test_update_user(test_app, monkeypatch):
       class AttrDict(dict):
           def __init__(self, *args, **kwargs):
               super(AttrDict, self).__init__(*args, **kwargs)
               self.__dict__ = self
       def mock_get_user_by_id(user_id):
           d = AttrDict()
           d.update({
               "id": 1,
               "username": "me",
               "email": "me@testdriven.io"
           })
           return d
       def mock_update_user(user, username, email):
           return True
       monkeypatch.setattr(project.api.users, "get_user_by_id", mock_get_user_by_id)
       monkeypatch.setattr(project.api.users, "update_user", mock_update_user)
       client = test_app.test_client()
       resp_one = client.put(
           "/users/1",
           data=json.dumps({"username": "me", "email": "me@testdriven.io"}),
           content_type="application/json",
       )
       data = json.loads(resp_one.data.decode())
       assert resp_one.status_code == 200
       assert "1 was updated!" in data["message"]
       resp_two = client.get("/users/1")
       data = json.loads(resp_two.data.decode())
       assert resp_two.status_code == 200
       assert "me" in data["username"]
       assert "me@testdriven.io" in data["email"]


   @pytest.mark.parametrize(
       "user_id, payload, status_code, message",
       [
           [1, {}, 400, "Input payload validation failed"],
           [1, {"email": "joseba@gmail.com"}, 400, "Input payload validation failed"],
           [
               999,
               {"username": "joseba", "email": "joseba@gmail.com"},
               404,
               "User 999 does not exist",
           ],
       ],
   )
   def test_update_user_invalid(test_app, monkeypatch, user_id, payload, status_code, message):
       def mock_get_user_by_id(user_id):
           return None
       monkeypatch.setattr(project.api.users, "get_user_by_id", mock_get_user_by_id)
       client = test_app.test_client()
       resp = client.put(
           f"/users/{user_id}", data=json.dumps(payload), content_type="application/json",
       )
       data = json.loads(resp.data.decode())
       assert resp.status_code == status_code
       assert message in data["message"]

Como no accedemos a la base de datos podemos ejecutar los test en paralelo y comprobar asi mas rapido nuestro componentes software. Para ello instalamos la dependencia *pytest-xdist==1.30.0*. Creamos el contenedor  y ejecutamos

.. code-block:: bash

   docker-compose exec users pytest "project/test/test_users_unit.py" -p no:warnings -k "unit" -n 4
