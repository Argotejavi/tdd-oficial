# TDD and Continuous integration  with docker, python flask, gitlab and heroku: EXTRAS

En este apartado se creará documentación Swagger del API y se activará una interfaz para la gestión del modelo de base de datos.


Comencemos instalando la interfaz web. Para ello lo primero es agregar la dependencia de *FlaskAdmin*. 

Para esto nos tenemos que asegurar que existe la base de datos. docker-compose exec users python manage.py recreate_db.

```
Flask-Admin==1.5.4
```

El siguiente paso es agregar la aplicación web en nuestro contexto de Flask. Esto lo haremos en *project/__init__.py*. Esto solo lo agregaremos en la versión de development.

```python
from flask_admin import Admin  



admin = Admin(template_mode="bootstrap3")

 if os.getenv("FLASK_ENV") == "development":
        admin.init_app(app)

```

Por último agregaremos la vista al modelo de Users en *project/api/models.py*.

```python
import os
from flask_admin.contrib.sqla import ModelView

if os.getenv("FLASK_ENV") == "development":
    from project import admin
    admin.add_view(ModelView(User, db.session))
```

Y ahora arrancamos las imágenes y creamos las tablas, y probamos a navegar.

```bash
docker-compose up -d --build
docker-compose exec users python manage.py recreate_db
```
Ahora podemos probar la vista de administración *http://10.100.199.200:5001/admin/user/*. Por último agregamos un test  que nos asegure que en producción no se agrega *FlaskAdmin*. Para estos test creamos un nuevo fichero y validamos que si navegamos a la url de la vista de usuarios recibimos un 200 de HTTP si estamos en modo *development* y que recibimos un 404 si estamos en modo producción.

```python
import os

from project import create_app, db


def test_admin_view_dev():
    os.environ["FLASK_ENV"] = "development"
    assert os.getenv("FLASK_ENV") == "development"
    app = create_app()
    app.config.from_object("project.config.TestingConfig")
    with app.app_context():
        db.session.remove()
        db.drop_all()
        db.create_all()
        client = app.test_client()
        resp = client.get("/admin/user/")
        assert resp.status_code == 200
    assert os.getenv("FLASK_ENV") == "development"


def test_admin_view_prod():
    os.environ["FLASK_ENV"] = "production"
    assert os.getenv("FLASK_ENV") == "production"
    app = create_app()
    app.config.from_object("project.config.TestingConfig")
    with app.app_context():
        db.session.remove()
        db.drop_all()
        db.create_all()
        client = app.test_client()
        resp = client.get("/admin/user/")
        assert resp.status_code == 404
    assert os.getenv("FLASK_ENV") == "production"
```
Para probar esto último en el pipeline de CI/CD tendremos que indicarle a la variable de entorno que estamos en 'development'.

```yaml
#.gitlab-ci.yaml
 FLASK_ENV: development  
```


Ahora vamos a agregarle la documentación swagger del API REST. Si ahora navegamos a la siguiente URL http://10.100.199.200:5001/ veremos la documentación del API del servicio PING, el cual es el namespace por defecto. Esta documentación no la genera la librería RESTPLUS de Flask. Para poder agregar también la documentación del API USERS tenemos que modificar ligeramente el código y utilizar namespaces en lugar de Blueprints. Para ello lo primero que hacemos es modificar el fichero del modulo API *__init__.py*.

```python
#project/api/__init__.oy
from flask_restplus import Api
api = Api(version="1.0", title="Users API")
```

El siguiente paso es actualizar el modulo principal del proyecto. El objetivo es quitar los blueprints y trabajar con namespaces. para ello hacemos  primero comentamos las líneas que registran los Blueprints y registramos el modulo entero de API de golpe.

```python
# project/__init__.py

# SE COMENTATN LAS SIGUIENTES LINEAS
#from project.api.ping import ping_blueprint
#app.register_blueprint(ping_blueprint)

#from project.api.users import users_blueprint
#app.register_blueprint(users_blueprint)

# Y SE AGREGA
 # register api
from project.api import api  
api.init_app(app)  
```

El siguiente paso es modificar los Blueprints para que trabajen con namespaces.

```python
# project/api/ping.py

from flask_restplus import Api, Resource, Namespace

 # COMENTAMOS
# ping_blueprint = Blueprint("ping", __name__)
# api = Api(ping_blueprint)

#AGREGAMOS
ping_namespace = Namespace("ping")  

# api.add_resource(Ping, "/ping")
ping_namespace.add_resource(Ping, "")
```

Y hacemos lo mismo con Users.

```python
# project/api/users/views.py
from flask_restplus import Api, Resource, fields, Namespace


#users_blueprint = Blueprint("users", __name__)
#api = Api(users_blueprint)
users_namespace = Namespace("users")



#user = api.model(
user = users_namespace.model(

#@api.expect(user, validate=True) 
@users_namespace.expect(user, validate=True) 

#@api.marshal_with(user, as_list=True)
@users_namespace.marshal_with(user, as_list=True)

#@api.marshal_with(
@users_namespace.marshal_with(
    user
#@api.expect(user, validate=True)
@users_namespace.expect(user, validate=True)

#Y MODIFICAR LAS RESÙESTA ERRPNEAS
#api.abort(404, f"User {user_id} does not exist")
users_namespace.abort(404, f"User {user_id} does not exist")
#api.abort(404, f"User {user_id} does not exist")
users_namespace.abort(404, f"User {user_id} does not exist")
#api.abort(404, f"User {user_id} does not exist")
users_namespace.abort(404, f"User {user_id} does not exist")

#api.add_resource(Users, "/users/<int:user_id>")
#api.add_resource(UsersList, "/users")

users_namespace.add_resource(UsersList, "")  
users_namespace.add_resource(Users, "/<int:user_id>")  
```

Finalmente habrá que agregar en el modulo API  los namespaces creados. Y una vez realizado esto ya podemos volver a ejecutar y probar nuestra documentación y aplicación ;-)

```python
# project/api/__init__.py
from project.api.ping import ping_namespace
from project.api.users import users_namespace

api.add_namespace(ping_namespace, path="/ping")  
api.add_namespace(users_namespace, path="/users")  
```

Y ahora si ejecutamos los test todo debería de ir correctamente. Y si navegamos a la URL http://10.100.199.200:5001/ veremos la documentación tanto de **/ping** como de **/users**

para finalizar configuraremos el namespace para que nos genere la documentación del API en la url */doc*. Para ello en el modulo *api* utilizamos el siguiente código.

```python
api = Api(version="1.0", title="Users API",  doc="/doc/")
```

Y ahora agregamos ciertos comentarios y decoradores para enriquecer la documentación  del API /users ;-)

```python
# project/api/users.py

 def get(self):
        """Returns all users."""  
@users_namespace.response(201, "<user_email> was added!")  # new
@users_namespace.response(400, "Sorry. That email already exists.")  # new
def post(self)



    @users_namespace.response(200, "Success")  
    @users_namespace.response(404, "User <user_id> does not exist")  
    def get(self, user_id):
        """Returns a single user."""
    
    @users_namespace.response(200, "<user_is> was updated!")  
    @users_namespace.response(404, "User <user_id> does not exist")  
    def put(self, user_id):
        """Updates a user.""" 
    @users_namespace.response(200, "<user_is> was removed!")  
    @users_namespace.response(404, "User <user_id> does not exist") 
    def delete(self, user_id):
        """Deletes a user.""" 

```

Y ahora tenemos una documentación muy completa!!!!!!!!!!!
Verificar y validar todo otra vez y volver a subirlo a PRODUCCION 
;-)))))))