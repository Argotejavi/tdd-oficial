
Extras
======

En este apartado se creara documnetacion Swagger del API y se activara una interfaz par ala gestion del modelo de base de datos.

Comencemos instalando la interfaz web. Para ello lo primero es agregar la dependencia de *FlaskAdmin*. 

Para esto nos tenemos que asegurar que existe la base de datos. docker-compose exec users python manage.py recreate_db.

.. code-block::

   Flask-Admin==1.5.4

El siguiente paso es agregar la aplicacion web en  nuestro contexto de Flask. esto lo haremos en *project/\ **init**.py*.Esto solo lo agregaremos enla version de development.

.. code-block:: python

   from flask_admin import Admin  



   admin = Admin(template_mode="bootstrap3")

    if os.getenv("FLASK_ENV") == "development":
           admin.init_app(app)

Por ultimo agregaremos la vista al modelo de Users en *project/api/models.py*.

.. code-block:: python

   import os

   if os.getenv("FLASK_ENV") == "development":
       from project import admin
       admin.add_view(ModelView(User, db.session))

Y ahora arrancamos las imagenes y creamos las tablas, y probamos a navegar.

.. code-block:: bash

   docker-compose up -d --build
   docker-compose exec users python manage.py recreate_db

Ahora podemos probar la vista de amdinistracion *http://10.100.199.200:5001/admin/user/*. Por ultimo agregamos un test  que nos asegure que en producción no se agrega *FlaskAdmin*. Para estos test creamos un nuveo fichero  y validamos que si navegamos a la url de la vista de usuarios recibimos un 200 de HTTP si estamos en modo *development* y que recibimos un 404 si estamos en modo produccion.

.. code-block:: python

   import os

   from project import create_app, db


   def test_admin_view_dev():
       os.environ["FLASK_ENV"] = "development"
       assert os.getenv("FLASK_ENV") == "development"
       app = create_app()
       app.config.from_object("project.config.TestingConfig")
       with app.app_context():
           db.session.remove()
           db.drop_all()
           db.create_all()
           client = app.test_client()
           resp = client.get("/admin/user/")
           assert resp.status_code == 200
       assert os.getenv("FLASK_ENV") == "development"


   def test_admin_view_prod():
       os.environ["FLASK_ENV"] = "production"
       assert os.getenv("FLASK_ENV") == "production"
       app = create_app()
       app.config.from_object("project.config.TestingConfig")
       with app.app_context():
           db.session.remove()
           db.drop_all()
           db.create_all()
           client = app.test_client()
           resp = client.get("/admin/user/")
           assert resp.status_code == 404
       assert os.getenv("FLASK_ENV") == "production"

Para probar esto ultimo en el pipeline de CI/CD tendremos que indicarle a la varibale de entorno que estamos en 'development'.

.. code-block:: yaml

   #.gitlab-ci.yaml
    FLASK_ENV: development

Ahora vamos a agregarle la documentacion swagger del API REST. Si haora navegamos a la siguiente URL http://10.100.199.200:5001/ veremos la documentacion del API del servicio PING, el cual es el namespace por defecto. Esta documnetacion no la genera la libreria RESTPLUS de Flask. Para poder agregar tambien la documentacion del API USERS tenemos que modificar ligeramente le codigo yutilziar namespaces en lugar de Blueprints. Para ello lo primero que hacemos es modificar el fichero del modulo API **_init**.py*.

.. code-block:: python

   #project/api/__init__.oy
   from flask_restplus import Api
   api = Api(version="1.0", title="Users API")

El siguienta paso es actualizar el modulo principal del proyecto. El objetivo es quitar los blueprints y trabajar con namespaces. para ello hacemos  primero comentamos las lineas que regitsran los Blueprints y regsitramos el modulo entero de API de golpe.

.. code-block:: python

   # project/__init__.py

   # SE COMENTATN LAS SIGUIENTES LINEAS
   #from project.api.ping import ping_blueprint
   #app.register_blueprint(ping_blueprint)

   #from project.api.users import users_blueprint
   #app.register_blueprint(users_blueprint)

   # Y SE AGREGA
    # register api
   from project.api import api  
   api.init_app(app)

El siguiente paso es modificar los Blueprints para que trabajen con namespaces.

.. code-block:: python

   # project/api/ping.py

   from flask_restplus import Api, Resource, Namespace

    # COMENTAMOS
   # ping_blueprint = Blueprint("ping", __name__)
   # api = Api(ping_blueprint)

   #AGREGAMOS
   ping_namespace = Namespace("ping")  

   # api.add_resource(Ping, "/ping")
   ping_namespace.add_resource(Ping, "")

Y hacemos lo mismo con Users.

.. code-block:: python

   # project/api/users/views.py
   from flask_restplus import Api, Resource, fields, Namespace


   #users_blueprint = Blueprint("users", __name__)
   #api = Api(users_blueprint)
   users_namespace = Namespace("users")



   #user = api.model(
   user = users_namespace.model(

   #@api.expect(user, validate=True) 
   @users_namespace.expect(user, validate=True) 

   #@api.marshal_with(user, as_list=True)
   @users_namespace.marshal_with(user, as_list=True)

   #@api.marshal_with(
   @users_namespace.marshal_with(
       user
   #@api.expect(user, validate=True)
   @users_namespace.expect(user, validate=True)

   #Y MODIFICAR LAS RESÙESTA ERRPNEAS
   #api.abort(404, f"User {user_id} does not exist")
   users_namespace.abort(404, f"User {user_id} does not exist")
   #api.abort(404, f"User {user_id} does not exist")
   users_namespace.abort(404, f"User {user_id} does not exist")
   #api.abort(404, f"User {user_id} does not exist")
   users_namespace.abort(404, f"User {user_id} does not exist")

   #api.add_resource(Users, "/users/<int:user_id>")
   #api.add_resource(UsersList, "/users")

   users_namespace.add_resource(UsersList, "")  
   users_namespace.add_resource(Users, "/<int:user_id>")

Finalmente habra que agregar en el modulo API  los namespaces creados. Y una vez realziado esto ya podemos volver a ejecutar y probar nuestra documentacion y aplicacion ;-)

.. code-block:: python

   # project/api/__init__.py
   from project.api.ping import ping_namespace
   from project.api.users import users_namespace

   api.add_namespace(ping_namespace, path="/ping")  
   api.add_namespace(users_namespace, path="/users")

Y ahora si ejecutamos los test todo deberia de ir correctamente. Y si navegamos a la URL http://10.100.199.200:5001/ veremos la documentacion tanto de **/ping** como de **/users**

para finalizar configuraremos el namespace para que nos genere la documentacion del API en la url */doc*. Para ello en eñl modulo *api* utilziamos el siguiente código.

.. code-block:: python

   api = Api(version="1.0", title="Users API",  doc="/doc/")

Y ahora agregamos ciertos comentarios y decoradores para enriquecer la documentacion  del API /users ;-)

.. code-block:: python

   # project/api/users.py

    def get(self):
           """Returns all users."""  
   @users_namespace.response(201, "<user_email> was added!")  # new
   @users_namespace.response(400, "Sorry. That email already exists.")  # new
   def post(self)



       @users_namespace.response(200, "Success")  
       @users_namespace.response(404, "User <user_id> does not exist")  
       def get(self, user_id):
           """Returns a single user."""

       @users_namespace.response(200, "<user_is> was updated!")  
       @users_namespace.response(404, "User <user_id> does not exist")  
       def put(self, user_id):
           """Updates a user.""" 
       @users_namespace.response(200, "<user_is> was removed!")  
       @users_namespace.response(404, "User <user_id> does not exist") 
       def delete(self, user_id):
           """Deletes a user."""

Y ahora tenemos una documentacion muy completa!!!!!!!!!!!
Verificar y validar todo otra vez y volver a subirlo a PRODUCCION 
;-)))))))
