.. Laboratorio TDD documentation master file, created by
   sphinx-quickstart on Thu Feb 11 17:54:08 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Laboratorio TDD's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   GIDA_00_HASIERAKETA
   GIDA_01_DOCKERiZAR
   GIDA_02_Instalando_BBDD
   GIDA_03_PyTest
   GIDA_04_blueprintf_refactor
   GIDA_05_tdd
   GIDA_06_DEPLOY
   GIDA_07_coverage
   GIDA_08_CI
   GIDA_09_CD
   GIDA_10_newAPI
   GIDA_11_mock
   GIDA_12


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
