# project/__init__.py

import sys
import os


from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin

# Instanciamos la aplicacion

db = SQLAlchemy()

admin = Admin(template_mode="bootstrap3")


def create_app(script_info=None):
    app = Flask(__name__)
    # configuramos
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)  # new

    db.init_app(app)

    if os.getenv("FLASK_ENV") == "development":
        admin.init_app(app)

    from project.api import api

    api.init_app(app)
    # from project.api.ping import ping_blueprint

    # app.register_blueprint(ping_blueprint)

    # from project.api.users import users_blueprint

    # app.register_blueprint(users_blueprint)

    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    print(app.config, file=sys.stderr)

    return app
