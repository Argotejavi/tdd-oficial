# from flask import Blueprint
# from flask_restplus import Api, Resource, Namespace
from flask_restplus import Resource, Namespace

# ping_blueprint = Blueprint("ping", __name__)
# api = Api(ping_blueprint)

ping_namespace = Namespace("ping")


class Ping(Resource):
    def get(self):
        return {"status": "ondo", "message": "pong"}


# api.add_resource(Ping, "/ping")

ping_namespace.add_resource(Ping, "")
