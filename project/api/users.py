# from flask import Blueprint, request
from flask import request

# from flask_restplus import Api, Resource, fields, Namespace
from flask_restplus import Resource, fields, Namespace

from project import db
from project.api.services import (
    add_user,
    delete_user,
    get_all_users,
    get_user_by_email,
    get_user_by_id,
    update_user,
)

# users_blueprint = Blueprint("users", __name__)
# api = Api(users_blueprint)
users_namespace = Namespace("users")


# user = api.model(
user = users_namespace.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "username": fields.String(required=True),
        "email": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


class UsersList(Resource):
    # @api.expect(user, validate=True)
    @users_namespace.expect(user, validate=True)
    def post(self):
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = get_user_by_email(email)  # User.query.filter_by(email=email).first()
        if user:
            response_object["message"] = "Sorry. That email already exists"
            return response_object, 400

        # Con el validador si lo sdatso no cumplen con el modelo no llega aqui
        try:
            # db.session.add(User(username=username, email=email))
            # db.session.commit()
            add_user(username, email)
            response_object["message"] = f"{email} was added!"
            return response_object, 201
        except Exception:
            db.session.rollback()
            response_object["message"] = f"Input payload validation failed"
            return response_object, 400

    # @api.marshal_with(user, as_list=True)
    @users_namespace.marshal_with(user, as_list=True)
    @users_namespace.response(201, "<user_email> was added!")
    @users_namespace.response(400, "Sorry. That email already exists.")
    def get(self):
        """Returns all users."""
        return get_all_users(), 200  # User.query.all(), 200


class Users(Resource):
    # @api.marshal_with(
    @users_namespace.marshal_with(
        user
    )  # este decorador se utiliza para pasar los datso del get a un JSON
    @users_namespace.response(200, "Success")
    @users_namespace.response(404, "User <user_id> does not exist")
    def get(self, user_id):
        """Returns a single user."""
        user = get_user_by_id(user_id)  # User.query.filter_by(id=user_id).first()
        if not user:
            # api.abort(404, f"User {user_id} does not exist")
            users_namespace.abort(404, f"User {user_id} does not exist")
        return user, 200

    @users_namespace.response(200, "<user_is> was removed!")
    @users_namespace.response(404, "User <user_id> does not exist")
    def delete(self, user_id):
        """Deletes a user."""
        response_object = {}
        user = get_user_by_id(user_id)  # User.query.filter_by(id=user_id).first()
        if not user:
            # api.abort(404, f"User {user_id} does not exist")
            users_namespace.abort(404, f"User {user_id} does not exist")
        # db.session.delete(user)
        # db.session.commit()
        delete_user(user)
        response_object["message"] = f"{user.email} was removed!"
        return response_object, 200

    # @api.expect(user, validate=True)
    @users_namespace.expect(user, validate=True)
    @users_namespace.response(200, "<user_is> was updated!")
    @users_namespace.response(404, "User <user_id> does not exist")
    def put(self, user_id):
        """Updates a user."""
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = get_user_by_id(user_id)  # User.query.filter_by(id=user_id).first()
        if not user:
            # api.abort(404, f"User {user_id} does not exist")
            users_namespace.abort(404, f"User {user_id} does not exist")
        # user.username = username
        # user.email = email
        # db.session.commit()
        update_user(user, username, email)
        response_object["message"] = f"{user.id} was updated!"
        return response_object, 200


# api.add_resource(Users, "/users/<int:user_id>")
# api.add_resource(UsersList, "/users")

users_namespace.add_resource(UsersList, "")
users_namespace.add_resource(Users, "/<int:user_id>")
