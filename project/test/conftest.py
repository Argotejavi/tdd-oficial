# project/test/conftest.py

import os

import pytest

from project import create_app, db
from project.api.models import User


# esto habria que hacerlo en un session o con un if dependiendo del tipo de test
@pytest.fixture(scope="session", autouse=True)
def test_debug():
    test_type = os.getenv("REMOTE_DEBUG")
    if test_type == "True":
        import ptvsd

        ptvsd.enable_attach(address=("0.0.0.0", 14001))
        ptvsd.wait_for_attach()


@pytest.fixture(scope="module")
def test_app():
    app = create_app()
    app.config.from_object("project.config.TestingConfig")
    with app.app_context():
        yield app


@pytest.fixture(scope="module")
def test_database():
    db.create_all()
    yield db
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="function")
def add_user():
    def _add_user(username, email):
        user = User(username=username, email=email)
        db.session.add(user)
        db.session.commit()
        return user

    return _add_user
